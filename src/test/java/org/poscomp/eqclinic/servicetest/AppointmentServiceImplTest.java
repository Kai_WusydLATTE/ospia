package org.poscomp.eqclinic.servicetest;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.service.interfaces.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(locations = {"classpath:spring-mvc.xml", "classpath:applicationContext.xml",
"classpath:applicationContext-hibernate.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AppointmentServiceImplTest {

    @Autowired
    private AppointmentService appointmentService;

    @Test
    public void testSaveApp() {
        Appointment app = new Appointment();
        app.setAptdate(new Date());
        appointmentService.saveApp(app);
    }
    
}
