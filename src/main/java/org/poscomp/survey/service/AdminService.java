package org.poscomp.survey.service;

import org.poscomp.survey.domain.Admin;

public interface AdminService {

    public void save(Admin admin);

    public Admin findByUsernameAndPassword(String username, String password);

    public boolean isExistByUsername(String username);

    public boolean isExistByEmail(String email);

    public Admin findById(int id);
    
    public Admin adminLogin(String username, String password);
}