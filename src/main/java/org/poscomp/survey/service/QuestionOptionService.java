package org.poscomp.survey.service;

import java.util.List;

import org.poscomp.survey.domain.QuestionOption;

public interface QuestionOptionService {

    public int saveQustionOption(QuestionOption option);
    
    public List<Integer> saveQustionOptions(List<QuestionOption> options);
    
    
}
