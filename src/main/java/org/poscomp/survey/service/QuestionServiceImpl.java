package org.poscomp.survey.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.poscomp.survey.dao.QuestionDao;
import org.poscomp.survey.dao.QuestionOptionDao;
import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.QuestionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("questionService")
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private QuestionOptionDao questionOptionDao;


    public void save(Question question) {

        questionDao.save(question);
        List<QuestionOption> questionOptions = question.getOptions();

        if (questionOptions != null) {
            String orderOfOptions = "";

            for (QuestionOption option : questionOptions) {
                int id = option.getId();
                if ("".equals(orderOfOptions)) {
                    orderOfOptions += id;
                } else {
                    orderOfOptions += "," + id;
                }
            }
            question.setOrderOfOptions(orderOfOptions);
            questionDao.save(question);
        }
    }
    

    public Question findById(int id) {
        return questionDao.findById(id);
    }

    public void delete(Question question) {
        questionDao.delete(question);
    }

    public void deleteById(int id) {
        this.delete(questionDao.findById(id));
    }

    @Override
    public List<Question> findAll(int pageNo, int pageSize) {
        return questionDao.findAll(pageNo, pageSize);

    }

    @Override
    public List<Question> findByIds(List<Integer> ids) {
        List<Question> result = new ArrayList<Question>();
        for (Integer id : ids) {
            result.add(findById(id));
        }
        return result;
    }

    @Override
    public List<Question> findByContent(String content, String ids) {
        return questionDao.findByContent(content, ids);
    }
}
