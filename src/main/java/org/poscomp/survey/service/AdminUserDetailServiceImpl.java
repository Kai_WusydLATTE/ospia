package org.poscomp.survey.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.DoctorDao;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.poscomp.survey.dao.AdminDao;
import org.poscomp.survey.domain.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service
public class AdminUserDetailServiceImpl implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(AdminUserDetailServiceImpl.class);

    @Autowired
    private AdminDao adminDao;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        MyUserPrincipal user = null;  
        
        try {  
            Admin admin = adminDao.findByName(username);
            if(admin==null){
                return null;
            }
            List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);  
            authList.add(new GrantedAuthorityImpl("ROLE_ADMIN"));  
            user = new MyUserPrincipal(admin.getUsername(), admin.getPassword().toLowerCase(),authList, null, null, null, admin);  
        } catch (Exception e) {  
            e.printStackTrace();
            logger.error("Error in retrieving user");  
            throw new UsernameNotFoundException("Error in retrieving user");  
        }  
  
        return user;  
        
    }
    
}
