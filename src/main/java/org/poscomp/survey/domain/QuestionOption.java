package org.poscomp.survey.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_questionoption", catalog = "ospia")
public class QuestionOption implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    private String optkey;
    private String optvalue;
    
    @ManyToOne
    @JoinColumn(name = "questionid")
    private Question question;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOptkey() {
        return optkey;
    }

    public void setOptkey(String optkey) {
        this.optkey = optkey;
    }

    public String getOptvalue() {
        return optvalue;
    }

    public void setOptvalue(String optvalue) {
        this.optvalue = optvalue;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
