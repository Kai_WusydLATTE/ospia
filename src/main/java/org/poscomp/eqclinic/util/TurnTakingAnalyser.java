package org.poscomp.eqclinic.util;

import java.util.ArrayList;
import java.util.Collections;

import org.poscomp.eqclinic.domain.pojo.SpeakingPeriod;
import org.poscomp.eqclinic.domain.pojo.TurnTakingInfo;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TurnTakingAnalyser {

    public ArrayList<SpeakingPeriod> turntakingAnalysis(String doctorEndpointsStr, String patientEndpointsStr) {

        ArrayList<SpeakingPeriod> sps = new ArrayList<SpeakingPeriod>();

        // add the patient speaking period
        String[] patientEndpoints = patientEndpointsStr.split(";");
        for (String patientEndpoint : patientEndpoints) {
            String[] points = patientEndpoint.split(",");
            if (points.length > 1) {
                float startPoint = Float.parseFloat(points[0]);
                float endPoint = Float.parseFloat(points[1]);
                SpeakingPeriod sp = new SpeakingPeriod();
                sp.setEndPointEnd(endPoint);
                sp.setEndPointStart(startPoint);
                sp.setSubject("patient");
                sp.setPeriod(endPoint - startPoint);
                sps.add(sp);
            }
        }
        // add the doctor speaking period
        String[] doctorEndpoints = doctorEndpointsStr.split(";");
        for (String doctorEndpoint : doctorEndpoints) {
            String[] points = doctorEndpoint.split(",");
            if (points.length > 1) {
                float startPoint = Float.parseFloat(points[0]);
                float endPoint = Float.parseFloat(points[1]);
                SpeakingPeriod sp = new SpeakingPeriod();
                sp.setEndPointEnd(endPoint);
                sp.setEndPointStart(startPoint);
                sp.setSubject("doctor");
                sp.setPeriod(endPoint - startPoint);
                sps.add(sp);
            }
        }

        // sort all speaking period
        Collections.sort(sps);

        // get all turn taking number
        int totalTurntaking = 0;
        SpeakingPeriod init = new SpeakingPeriod();
        for (int i = 0; i < sps.size(); i++) {
            if (i == 0) {
                init = sps.get(i);
            } else {
                if (!init.getSubject().equals(sps.get(i).getSubject())) {
                    totalTurntaking++;
                    init = sps.get(i);
                }
            }
        }
        return sps;

    }

    public ArrayList<SpeakingPeriod> shrink(ArrayList<SpeakingPeriod> sp) {

        ArrayList<SpeakingPeriod> sps = new SpeakingPeriod().copy(sp);

        ArrayList<SpeakingPeriod> turns = new ArrayList<SpeakingPeriod>();
        SpeakingPeriod init = new SpeakingPeriod();
        for (int i = 0; i < sps.size(); i++) {
            if (i == 0) {
                turns.add(sps.get(i));
                init = sps.get(i);
            } else {
                if (init.getSubject().equals(sps.get(i).getSubject())) {
                    int currrentSize = turns.size();
                    turns.get(currrentSize - 1)
                                    .setPeriod(turns.get(currrentSize - 1).getPeriod() + sps.get(i).getPeriod());
                } else {
                    turns.add(sps.get(i));
                    init = sps.get(i);
                }
            }
        }

        return turns;
    }

    public ArrayList<TurnTakingInfo> extractTurnTakingInfo(ArrayList<SpeakingPeriod> sps,
                    ArrayList<SpeakingPeriod> originalsps) {
        float totalTime = 0;
        float doctorTime = 0;
        float patientTime = 0;
        int doctorTurn = 0;
        int patientTurn = 0;
        float longestTurnDoctor = 0;
        float longestTurnPatient = 0;
        float longestTurnDoctorStartTime = 0;
        float longestTurnPatientStartTime = 0;
        float shortestTurnDoctor = 10000;
        float shortestTurnPatient = 10000;
        int responseTimeDoctor = 0;
        int responseTimePatient = 0;
        float totalResponseTimeDoctor = 0;
        float totalResponseTimePatient = 0;

        for (SpeakingPeriod sp : sps) {
            totalTime += sp.getPeriod();
            if ("patient".equals(sp.getSubject())) {
                patientTime += sp.getPeriod();
                patientTurn++;
                if (sp.getPeriod() > longestTurnPatient) {
                    longestTurnPatient = sp.getPeriod();
                    longestTurnPatientStartTime = sp.getEndPointStart();
                }
                if (sp.getPeriod() < shortestTurnPatient) {
                    shortestTurnPatient = sp.getPeriod();
                }
            } else {
                doctorTime += sp.getPeriod();
                doctorTurn++;
                if (sp.getPeriod() > longestTurnDoctor) {
                    longestTurnDoctor = sp.getPeriod();
                    longestTurnDoctorStartTime = sp.getEndPointStart();
                }
                if (sp.getPeriod() < shortestTurnDoctor) {
                    shortestTurnDoctor = sp.getPeriod();
                }
            }
        }

        float propotionDoctor = 0;
        float propotionPatient = 0;
        if (totalTime != 0) {
            propotionDoctor = doctorTime / totalTime;
            propotionPatient = patientTime / totalTime;
        }

        float averageLengthDoctor = 0;
        float averageLengthPatient = 0;
        if (doctorTurn != 0) {
            averageLengthDoctor = doctorTime / doctorTurn;
        }
        if (patientTurn != 0) {
            averageLengthPatient = patientTime / patientTurn;
        }

        float totalOverlapping = 0;
        // calculate overlapping time
        for (SpeakingPeriod sp : originalsps) {

            if ("patient".equals(sp.getSubject())) {

                float patientStart = sp.getEndPointStart();
                float patientEnd = sp.getEndPointEnd();

                for (SpeakingPeriod dsp : originalsps) {
                    if ("doctor".equals(dsp.getSubject())) {
                        float doctorStart = dsp.getEndPointStart();
                        float doctorEnd = dsp.getEndPointEnd();
                        totalOverlapping += calculateOverlapping(patientStart, patientEnd, doctorStart, doctorEnd);
                    }
                }
            }
        }

        float patientOverlappingTime = totalOverlapping;
        float doctorOverlappingTime = totalOverlapping;
        float patientOverlappingPercent = totalOverlapping / patientTime;
        float doctorOverlappingPercent = totalOverlapping / doctorTime;

        // calculate response time
        SpeakingPeriod previous = null;
        for (SpeakingPeriod speakingPeriod : originalsps) {
            if (previous == null) {
                previous = speakingPeriod;
            } else {
                if (!speakingPeriod.getSubject().equals(previous.getSubject())) {
                    if ("doctor".equals(speakingPeriod.getSubject())) {
                        responseTimeDoctor++;
                        totalResponseTimeDoctor += speakingPeriod.getEndPointStart() - previous.getEndPointEnd();
                    }
                    if ("patient".endsWith(speakingPeriod.getSubject())) {
                        responseTimePatient++;
                        totalResponseTimePatient += speakingPeriod.getEndPointStart() - previous.getEndPointEnd();
                    }
                }
                previous = speakingPeriod;
            }
        }
        float averageResponseTimeDoctor = 0;
        if (responseTimeDoctor != 0) {
            averageResponseTimeDoctor = totalResponseTimeDoctor / responseTimeDoctor;
        }

        float averageResponseTimePatient = 0;
        if (responseTimePatient != 0) {
            averageResponseTimePatient = totalResponseTimePatient / responseTimePatient;
        }

        ArrayList<TurnTakingInfo> infos = new ArrayList<TurnTakingInfo>();
        TurnTakingInfo docInfo = new TurnTakingInfo();
        docInfo.setPropotion(propotionDoctor);
        docInfo.setNumberOfTurn(doctorTurn);
        docInfo.setAverageLength(averageLengthDoctor);
        docInfo.setLogestTurn(longestTurnDoctor);
        docInfo.setLogestTurnStartTime(longestTurnDoctorStartTime);
        docInfo.setShortestTurn(shortestTurnDoctor);
        docInfo.setAverageResponeTime(averageResponseTimeDoctor);
        docInfo.setOverlappingTime(doctorOverlappingTime);
        docInfo.setOverlappingPercent(doctorOverlappingPercent);

        TurnTakingInfo patInfo = new TurnTakingInfo();
        patInfo.setPropotion(propotionPatient);
        patInfo.setNumberOfTurn(patientTurn);
        patInfo.setAverageLength(averageLengthPatient);
        patInfo.setLogestTurn(longestTurnPatient);
        patInfo.setLogestTurnStartTime(longestTurnPatientStartTime);
        patInfo.setShortestTurn(shortestTurnPatient);
        patInfo.setAverageResponeTime(averageResponseTimePatient);
        patInfo.setOverlappingTime(patientOverlappingTime);
        patInfo.setOverlappingPercent(patientOverlappingPercent);

        infos.add(docInfo);
        infos.add(patInfo);

        return infos;
    }

    public float calculateOverlapping(float obj1Start, float obj1End, float obj2Start, float obj2End) {
        if (obj1End <= obj2Start) {
            return 0;
        } else if (obj2End <= obj1Start) {
            return 0;
        } else {

            ArrayList<Float> temp = new ArrayList<Float>();
            temp.add(obj1Start);
            temp.add(obj1End);
            temp.add(obj2Start);
            temp.add(obj2End);
            Collections.sort(temp);
            // System.out.println("Interruption: " + temp.get(1) + ", " + Math.abs(temp.get(1) - temp.get(2)));
            return 1.0f;
//            return Math.abs(temp.get(1) - temp.get(2));
//            if(Math.abs(temp.get(1) - temp.get(2))>1){
//                System.out.println("Interruption: " + temp.get(1));
//                return 1.0f;
//            }
//            return 0;
        }

    }

}
