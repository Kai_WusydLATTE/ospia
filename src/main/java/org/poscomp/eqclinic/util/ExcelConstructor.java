package org.poscomp.eqclinic.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
 

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelConstructor {

    public static void main(String[] args) throws Exception {
        
        // ExcelConstructor ec = new ExcelConstructor();
        // ec.constructWeeklyReport();
        
    }
    
    public static void constructWeeklyReport(List<Object> spRegistration, List<Object> aptOffered, List<Object> aptCompleted,
                    List<Object> aptFailed, List<Object> aptCanceledStu, List<Object> aptCanceledSP,
                    List<Object> aptCompletedSP, List<Object> aptCompletedStu, List<Object> aptImappropriate,
                    ArrayList<Integer> aptConclusions){
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("1. SP registion");
         
        CellStyle boldStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);

        int rowCount = 0;
        Row row = sheet.createRow(rowCount);
        Cell cell = row.createCell(0);
        cell.setCellValue("First Name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("Last Name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("Date of registration");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(3);
        cell.setCellValue("Email");
        cell.setCellStyle(boldStyle);
        
        for (Object obj : spRegistration) {
            Object[] item = (Object[])obj;
            row = sheet.createRow(++rowCount);
            int columnCount = -1;
            for (Object field : item) {
                cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Long) {
                    String time = new SimpleDateFormat("dd-MM-yyyy HH:mm").format((Long) field);
                    cell.setCellValue(time);
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            sheet.autoSizeColumn(i);
        }
        
        
        sheet = workbook.createSheet("2. Apt offered");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("First Name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("Last Name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("No. of appointment (this includes available , requested, confrimed and finished appointment)");
        cell.setCellStyle(boldStyle);
        
        for (Object obj : aptOffered) {
            Object[] item = (Object[])obj;
            row = sheet.createRow(++rowCount);
            int columnCount = -1;
            for (Object field : item) {
                cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Long) {
                    cell.setCellValue((Long) field);
                }
            }
        }
        
        for (int i = 0; i < 3; i++) {
            sheet.autoSizeColumn(i);
        }
        
        sheet = workbook.createSheet("3. Apt completed");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("Stu first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("Stu last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("zid");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(3);
        cell.setCellValue("SP first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(4);
        cell.setCellValue("SP last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(5);
        cell.setCellValue("Appointment time");
        cell.setCellStyle(boldStyle);
        
        for (Object obj : aptCompleted) {
            Object[] item = (Object[])obj;
            row = sheet.createRow(++rowCount);
            int columnCount = -1;
            for (Object field : item) {
                cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Long) {
                    String time = new SimpleDateFormat("dd-MM-yyyy HH:mm").format((Long) field);
                    cell.setCellValue(time);
                }
            }
        }
        for (int i = 0; i < 6; i++) {
            sheet.autoSizeColumn(i);
        }
        
        sheet = workbook.createSheet("4. Apt failed");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("none");

        
        sheet = workbook.createSheet("5. Apt canceled");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("Student canceled");
        cell.setCellStyle(boldStyle);
        
        rowCount = 1;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("Stu first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("Stu last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("zid");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(3);
        cell.setCellValue("SP first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(4);
        cell.setCellValue("SP last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(5);
        cell.setCellValue("Appointment time");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(6);
        cell.setCellValue("Canceled reason");
        cell.setCellStyle(boldStyle);
        
        if(aptCanceledStu.size()==0){
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue("none");
        }else{
            for (Object obj : aptCanceledStu) {
                Object[] item = (Object[])obj;
                row = sheet.createRow(++rowCount);
                int columnCount = -1;
                for (Object field : item) {
                    cell = row.createCell(++columnCount);
                    if (field instanceof String) {
                        cell.setCellValue((String) field);
                    } else if (field instanceof Integer) {
                        cell.setCellValue((Integer) field);
                    } else if (field instanceof Long) {
                        String time = new SimpleDateFormat("dd-MM-yyyy HH:mm").format((Long) field);
                        cell.setCellValue(time);
                    }
                }
            }
        }
        
        rowCount++;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue(" ");
        cell.setCellStyle(boldStyle);
        rowCount++;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("SP canceled");
        cell.setCellStyle(boldStyle);
        
        rowCount++;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("Stu first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("Stu last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("zid");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(3);
        cell.setCellValue("SP first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(4);
        cell.setCellValue("SP last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(5);
        cell.setCellValue("Appointment time");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(6);
        cell.setCellValue("Canceled reason");
        cell.setCellStyle(boldStyle);
        
        if(aptCanceledSP.size()==0){
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue("none");
        }else{
            for (Object obj : aptCanceledSP) {
                Object[] item = (Object[])obj;
                row = sheet.createRow(++rowCount);
                int columnCount = -1;
                for (Object field : item) {
                    cell = row.createCell(++columnCount);
                    if (field instanceof String) {
                        cell.setCellValue((String) field);
                    } else if (field instanceof Integer) {
                        cell.setCellValue((Integer) field);
                    } else if (field instanceof Long) {
                        String time = new SimpleDateFormat("dd-MM-yyyy HH:mm").format((Long) field);
                        cell.setCellValue(time);
                    }
                }
            }
        }
        
        for (int i = 0; i < 7; i++) {
            sheet.autoSizeColumn(i);
        }
        
        sheet = workbook.createSheet("6. Apt completed (SP)");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("SP first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("SP last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("No. of completed");
        cell.setCellStyle(boldStyle);
        
        for (Object obj : aptCompletedSP) {
            Object[] item = (Object[])obj;
            row = sheet.createRow(++rowCount);
            int columnCount = -1;
            for (Object field : item) {
                cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Long) {
                    cell.setCellValue((Long) field);
                }
            }
        }
        
        for (int i = 0; i < 3; i++) {
            sheet.autoSizeColumn(i);
        }
        
        sheet = workbook.createSheet("7. Apt completed (Stu)");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("Student first name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(1);
        cell.setCellValue("Student last name");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(2);
        cell.setCellValue("zId");
        cell.setCellStyle(boldStyle);
        cell = row.createCell(3);
        cell.setCellValue("No. of completed");
        cell.setCellStyle(boldStyle);
        
        for (Object obj : aptCompletedStu) {
            Object[] item = (Object[])obj;
            row = sheet.createRow(++rowCount);
            int columnCount = -1;
            for (Object field : item) {
                cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Long) {
                    cell.setCellValue((Long) field);
                }
            }
        }
        
        for (int i = 0; i < 4; i++) {
            sheet.autoSizeColumn(i);
        }
        
        sheet = workbook.createSheet("8. Inappropriate");
        boldStyle = sheet.getWorkbook().createCellStyle();
        font = sheet.getWorkbook().createFont();
        font.setBold(true);
        boldStyle.setFont(font);
        rowCount = 0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(0);
        cell.setCellValue("none");
        
        try {
            SimpleDateFormat timeFormatShort = new SimpleDateFormat("ddMMyyyy");
            String dateShort = timeFormatShort.format(new Date());
            SimpleDateFormat timeFormat = new SimpleDateFormat("ddMMyyyy hh:mm:ss");
            Date endTimeDate = timeFormat.parse(dateShort+" 00:00:00)");
            long endTime = endTimeDate.getTime();
            long startTime = endTime - 7*24*3600*1000;
           
            sheet = workbook.createSheet("9.Conclusion numbers");
            boldStyle = sheet.getWorkbook().createCellStyle();
            font = sheet.getWorkbook().createFont();
            font.setBold(true);
            boldStyle.setFont(font);
            rowCount = 0;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            timeFormat = new SimpleDateFormat("dd/MM/yyyy");
            cell.setCellValue("By " + timeFormat.format(new Date()));
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue("Type");
            cell.setCellStyle(boldStyle);
            cell = row.createCell(1);
            cell.setCellValue("Number of appointment");
            cell.setCellStyle(boldStyle);
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue("Available appointments");
            cell = row.createCell(1);
            cell.setCellValue(aptConclusions.get(0));
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue("Confirmed appointments");
            cell = row.createCell(1);
            cell.setCellValue(aptConclusions.get(2));
            
            rowCount++;
            row = sheet.createRow(rowCount);
            cell = row.createCell(0);
            cell.setCellValue("Wait SPs to confirm appointments");
            cell = row.createCell(1);
            cell.setCellValue(aptConclusions.get(1));
            
            for (int i = 0; i < 2; i++) {
                sheet.autoSizeColumn(i);
            }
            
            String fileName = timeFormatShort.format(endTime);
            FileOutputStream outputStream;
            outputStream = new FileOutputStream("c:/weekly_report/weekly_report_"+fileName+".xlsx");
            workbook.write(outputStream);
            workbook.close();
            
            EmailTool.sendweeklyReport("c:/weekly_report/weekly_report_"+fileName+".xlsx");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
}
