package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.pojo.NoddingObject;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class ParserNodding {

    private static final Logger logger = Logger.getLogger(ParserNodding.class);

    public static void main(String[] args) throws InterruptedException {
        String fileNameDoctor = "video_1436761313842_doctor";

        // detect nodding and shaking
        ParserAllpoints ap = new ParserAllpoints();
        // get the nose point from the all points file
        ap.getNosePointFromAll(fileNameDoctor + "_allpoints.txt", fileNameDoctor + "_nosepoint.txt");

        ParserNodding tn = new ParserNodding();
        try {
            // // detect the nodding
            // tn.manipXYNodding(fileNameDoctor + "_nosepoint.txt",
            // fileNameDoctor + "_nodding.txt", 25.0f);
            // // merge the continuous nodding
            // tn.mergeNoddingAndShaking(fileNameDoctor + "_nodding.txt",
            // fileNameDoctor + "_noddingnew.txt");

            // detect the shaking
            tn.manipXYShaking(fileNameDoctor + "_nosepoint.txt", fileNameDoctor + "_shaking.txt", 25.0f);
            // merge the continuous nodding
            tn.mergeNoddingAndShaking(fileNameDoctor + "_shaking.txt", fileNameDoctor + "_shakingnew.txt");
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void deleteRepeat(String shakingFile, String noddingFile, String outShakingFile, String outNoddingFile) throws IOException {

        List<NoddingObject> noddings = new ArrayList<NoddingObject>();
        List<NoddingObject> shakings = new ArrayList<NoddingObject>();

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(new File(shakingFile)));
            String line = br.readLine();
            while (line != null && !"".equals(line)) {
                String[] temp = line.split(":");
                if (temp.length == 4) {
                    NoddingObject shakingObject = new NoddingObject();
                    shakingObject.setType(temp[0]);
                    shakingObject.setEndTime(Float.parseFloat(temp[1]));
                    shakingObject.setStartTime(Float.parseFloat(temp[2]));
                    shakingObject.setMoveValue(Float.parseFloat(temp[3]));
                    shakings.add(shakingObject);
                }
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        BufferedReader br1;
        try {
            br1 = new BufferedReader(new FileReader(new File(noddingFile)));
            String line = br1.readLine();
            while (line != null && !"".equals(line)) {
                String[] temp = line.split(":");
                if (temp.length == 4) {
                    NoddingObject noddingObject = new NoddingObject();
                    noddingObject.setType(temp[0]);
                    noddingObject.setEndTime(Float.parseFloat(temp[1]));
                    noddingObject.setStartTime(Float.parseFloat(temp[2]));
                    noddingObject.setMoveValue(Float.parseFloat(temp[3]));
                    noddings.add(noddingObject);

                }
                line = br1.readLine();
            }
            br1.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        List<NoddingObject> newNoddings = deleteRepeatObject(noddings, shakings);

        List<NoddingObject> newShakings = deleteRepeatObject(shakings, noddings);

        FileWriter writer1 = new FileWriter(outShakingFile);
        for (NoddingObject noddingObject : newShakings) {
            writer1.write("shaking:" + noddingObject.getEndTime() + ":" + noddingObject.getStartTime() + ":" + noddingObject.getMoveValue() + "\r\n");
        }
        writer1.close();

        FileWriter writer2 = new FileWriter(outNoddingFile);
        for (NoddingObject noddingObject : newNoddings) {
            writer2.write("nodding:" + noddingObject.getEndTime() + ":" + noddingObject.getStartTime() + ":" + noddingObject.getMoveValue() + "\r\n");
        }
        writer2.close();

    }

    public List<NoddingObject> deleteRepeatObject(List<NoddingObject> list1, List<NoddingObject> list2) {

        List<NoddingObject> result = new ArrayList<NoddingObject>();

        for (NoddingObject obj1 : list1) {
            boolean shouldDelete = false;

            if (obj1.getMoveValue() <= 50) {
                shouldDelete = true;
            } else {
                for (NoddingObject obj2 : list2) {
                    if (obj1.getStartTime() >= obj2.getEndTime() || obj1.getEndTime() <= obj2.getStartTime()) {

                    } else {
                        if (obj1.getMoveValue() < obj2.getMoveValue()) {
                            shouldDelete = true;
                        }
                    }
                }
            }
            if (shouldDelete == false) {
                result.add(obj1);
            }
        }

        return result;

    }

    /*
     * if there are several continuous(with in three seconds) nodding or
     * shaking, the repeated item will not include in the result
     */
    public void mergeNoddingAndShaking(String fileName, String outputFile) {
        BufferedReader br;
        BufferedWriter bw;

        float lastEndTime = 0f;
        float lastStartTime = 0f;
        try {
            br = new BufferedReader(new FileReader(new File(fileName)));
            bw = new BufferedWriter(new FileWriter(new File(outputFile)));
            String line = br.readLine();
            String type = "";
            float total = 0;
            while (line != null && !"".equals(line)) {

                String[] temp = line.split(":");
                if (lastEndTime == 0 && lastStartTime == 0) {
                    lastEndTime = Float.parseFloat(temp[1]);
                    lastStartTime = Float.parseFloat(temp[2]);
                    total = Float.parseFloat(temp[3]);
                    type = temp[0];
                } else {
                    if (Math.abs(lastEndTime - Float.parseFloat(temp[2])) < 1) {
                        lastEndTime = Float.parseFloat(temp[1]);
                        total += Float.parseFloat(temp[3]);
                    } else {
                        bw.write(type + ":" + lastEndTime + ":" + lastStartTime + ":" + total);
                        bw.newLine();
                        lastEndTime = Float.parseFloat(temp[1]);
                        lastStartTime = Float.parseFloat(temp[2]);
                        total = Float.parseFloat(temp[3]);
                    }
                }
                line = br.readLine();
            }

            br.close();
            bw.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /*
     * this function is used for detection nodding and shaking
     */
    public void manipXYNodding(String fileName, String outputFile, float frameRate) throws IOException {

        // List<Float> xPoints = Util2.readFileX(fileName);
        List<Float> yPoints = NoddingFileParser.readFileY(fileName);

        // List<String> labelsX = new ArrayList<String>();
        List<String> labelsY = new ArrayList<String>();

        FileWriter writer = new FileWriter(outputFile);

        // from the 3rd point to the 3rd last one
        for (int i = 4; i < yPoints.size() - 4; i = i + 2) {
            float[] pointsY = { yPoints.get(i - 4), yPoints.get(i - 2), yPoints.get(i), yPoints.get(i + 2), yPoints.get(i + 4) };

            if (yPoints.get(i) == 0) {
                continue;
            }

            String labelY = this.getLabel(i, pointsY);
            String frameY = i + ":" + labelY + ":" + yPoints.get(i);
//            System.out.println(frameY);
            labelsY.add(frameY);

            if ("s".equals(labelY)) {
                // check whether there is a nodding
                String isNodding = checkIsNodding(labelsY);
                if (!isNodding.equals("-1")) {
                    int frameNo = Integer.parseInt(isNodding.split(",")[0]);
                    float moveValue = Float.parseFloat(isNodding.split(",")[1]);
//                    System.out.println(("nodding:" + i / frameRate + ":" + frameNo / frameRate + ":" + moveValue));
                    writer.write("nodding:" + i / frameRate + ":" + frameNo / frameRate + ":" + moveValue + "\r\n");
                }
            }

        }
        writer.close();
    }

    /*
     * this function is used for detection nodding and shaking
     */
    public void manipXYShaking(String fileName, String outputFile, float frameRate) throws IOException {

        List<Float> xPoints = NoddingFileParser.readFileX(fileName);
        // List<Float> yPoints = Util2.readFileY(fileName);

        List<String> labelsX = new ArrayList<String>();
        // List<String> labelsY = new ArrayList<String>();

        FileWriter writer = new FileWriter(outputFile);

        // from the 3rd point to the 3rd last one
        for (int i = 4; i < xPoints.size() - 4; i = i + 2) {

            float[] pointsX = { xPoints.get(i - 4), xPoints.get(i - 2), xPoints.get(i), xPoints.get(i + 2), xPoints.get(i + 4) };

            if (xPoints.get(i) == 0) {
                continue;
            }

            String labelX = this.getLabel(i, pointsX);
            String frameX = i + ":" + labelX + ":" + xPoints.get(i);
            // System.out.println(frameX);
            labelsX.add(frameX);

            if ("s".equals(labelX)) {

                // check whether there is a nodding
                String isNodding = checkIsNodding(labelsX);
                if (!isNodding.equals("-1")) {
                    int frameNo = Integer.parseInt(isNodding.split(",")[0]);
                    float moveValue = Float.parseFloat(isNodding.split(",")[1]);

                    // System.out.println(("shaking:" + i / frameRate + ":" + frameNo / frameRate +":" + moveValue));
                    writer.write("shaking:" + i / frameRate + ":" + frameNo / frameRate + ":" + moveValue + "\r\n");

                }
            }

        }
        writer.close();
    }

    public String checkIsNodding(List<String> labels) {

        List<String> extremeValues = new ArrayList<String>();

        for (int i = labels.size() - 2; i >= 0; i--) {

            String frame = labels.get(i);
            String[] values = frame.split(":");


            if ("e".equals(values[1])) {

                if (extremeValues.size() > 0) {
                    String lastExtreme = extremeValues.get(extremeValues.size() - 1);
                    int lastFrameNo = Integer.parseInt(lastExtreme.split(":")[0]);
                    float lastFrameValue = Float.parseFloat(lastExtreme.split(":")[1]);
                    if (Math.abs(lastFrameNo - Integer.parseInt(values[0]))<=6
                                    && Math.abs(lastFrameValue - Float.parseFloat(values[2]))<=2) {

                    } else {
                        extremeValues.add(values[0] + ":" + Float.parseFloat(values[2]));
                    }

                } else {
                    extremeValues.add(values[0] + ":" + Float.parseFloat(values[2]));
                }

            } else if ("s".equals(values[1])) {
                if (extremeValues.size() >= 2 && extremeAdjacent(extremeValues) > 0) {
                    // return Integer.parseInt(values[0]);
                    return values[0] + "," + extremeAdjacent(extremeValues);
                } else {
                    return "-1";
                }

            }
        }
        return "-1";
    }

    public float extremeAdjacent(List<String> extremes) {

        for (int i = 0; i < extremes.size() - 1; i++) {
            float value1 = Float.parseFloat(extremes.get(i).split(":")[1]);
            float value2 = Float.parseFloat(extremes.get(i + 1).split(":")[1]);

            if (Math.abs(value1 - value2) <= 2) {
                return 0;
            }
        }

        float total = 0;
        for (int i = 0; i < extremes.size() - 1; i++) {
            float value1 = Float.parseFloat(extremes.get(i).split(":")[1]);
            float value2 = Float.parseFloat(extremes.get(i + 1).split(":")[1]);
            total += Math.abs(value1 - value2);
        }
        return total;

    }

    public String getLabel(int number, float[] yps) {
        if (yps.length == 5) {
            float min = this.getMin(yps);
            float max = this.getMax(yps);

            if ((max - min) <= 2) {
                return "s"; // label stable
            } else {
                // if this point is a polar point, then label it as e (extreme)
                if (yps[2] == max || yps[2] == min) {
                    return "e";
                } else {
                    return "t";
                }
            }
        } else {
            return null;
        }
    }

    public float getMax(float[] values) {
        float max = -1;

        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }

        return max;
    }

    public float getMin(float[] values) {
        float min = 10000;

        for (int i = 0; i < values.length; i++) {
            if (values[i] < min) {
                min = values[i];
            }
        }

        return min;
    }

}
