package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class ParserSmile {

    private static final Logger logger = Logger.getLogger(ParserSmile.class);
    
    public static void main(String[] args) {

        ParserSmile st = new ParserSmile();
        try {
            st.analysisSmile(
                            "C:/Program Files/apache-tomcat-7.0.55/webapps/roc/video/video_1423894669905_right_smile.txt",
                            "C:/Program Files/apache-tomcat-7.0.55/webapps/roc/video/video_1423894669905_right_smile1.txt",
                            5);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

    }

    public void analysisSmile(String inputFile, String outputFile, int frameRate) throws IOException {

        FileReader reader = new FileReader(new File(inputFile));
        BufferedReader br = new BufferedReader(reader);

        FileWriter writer = new FileWriter(new File(outputFile));
        BufferedWriter bw = new BufferedWriter(writer);

        String line = br.readLine();

        List<Integer> allLabel = new ArrayList<Integer>();

        while (line != null && !"".equals(line)) {
            allLabel.add(Math.round(Float.parseFloat(line)));
            line = br.readLine();
        }
        line = br.readLine();
        int secondNumber = allLabel.size() / frameRate;
        for (int i = 0; i < secondNumber; i++) {
            float result = 0;
            result = getTotalSmile(allLabel, i, frameRate)/25;
            
//            if (i == 0 || i == secondNumber - 1) {
//                result = getTotalSmile(allLabel, i, frameRate);
//            } else {
//                float pre = getTotalSmile(allLabel, i - 1, frameRate);
//                float cur = getTotalSmile(allLabel, i, frameRate);
//                float next = getTotalSmile(allLabel, i + 1, frameRate);
//                result = (pre + cur + next) / 3;
//            }

            // bw.write((i + 1) + ":" + (float) (int) (result * 100) / 100);
            bw.write((i + 1) + ":" + result);
            bw.newLine();
        }

        br.close();
        bw.close();

    }

    public int getTotalSmile(List<Integer> allLabels, int second, int frameRate) {

        int start = second * frameRate;
        int total = 0;
        for (int i = start; i < start + frameRate; i++) {
            total += allLabels.get(i);
        }
        return total;

    }
}
