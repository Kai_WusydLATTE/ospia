package org.poscomp.eqclinic.util;

import java.util.ArrayList;

import org.poscomp.eqclinic.domain.pojo.EndPoint;
import org.poscomp.eqclinic.domain.pojo.SpeechPercentage;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class SpeechPercentageTool {

    public static ArrayList<Float> getStopPoints(float total) {

        ArrayList<Float> stopPoints = new ArrayList<Float>();
        float interval = 1f;
        int totalStops = (int) (total / interval);
        for (int i = 0; i <= totalStops; i++) {
            stopPoints.add(interval * i);
        }
        stopPoints.add(total);
        return stopPoints;
    }

    public static ArrayList<SpeechPercentage> getSpeechPercentage(ArrayList<Float> stopPoints,
                    ArrayList<EndPoint> endPointsDoctor, ArrayList<EndPoint> endPointsPatient) {

        ArrayList<SpeechPercentage> sps = new ArrayList<SpeechPercentage>();

        for (float stopPoint : stopPoints) {
            SpeechPercentage sp = new SpeechPercentage();
            float totalSpeechDoctor = 0;
            float totalSpeechPatient = 0;

            if (stopPoint == 0) {
                sp.setTime(stopPoint);
                sp.setDoctorPercentage(0);
                sp.setPatientPercentage(0);
            } else {
                for (EndPoint endPoint : endPointsPatient) {
                    if (endPoint.getEnd() <= stopPoint) {
                        totalSpeechPatient += (endPoint.getEnd() - endPoint.getStart());
                    } else if (endPoint.getEnd() > stopPoint && endPoint.getStart() <= stopPoint) {
                        totalSpeechPatient += (stopPoint - endPoint.getStart());
                    }
                }

                for (EndPoint endPoint : endPointsDoctor) {
                    if (endPoint.getEnd() <= stopPoint) {
                        totalSpeechDoctor += (endPoint.getEnd() - endPoint.getStart());
                    } else if (endPoint.getEnd() > stopPoint && endPoint.getStart() <= stopPoint) {
                        totalSpeechDoctor += (stopPoint - endPoint.getStart());
                    }
                }
                sp.setTime(stopPoint);
                sp.setDoctorPercentage(totalSpeechDoctor);
                sp.setPatientPercentage(totalSpeechPatient);
            }
            sps.add(sp);
        }

        return sps;
    }


    public static ArrayList<SpeechPercentage> getSpeechPercentageSplit(ArrayList<Float> stopPoints,
                    ArrayList<EndPoint> endPointsDoctor, ArrayList<EndPoint> endPointsPatient, int splitSecond) {

        ArrayList<SpeechPercentage> sps = new ArrayList<SpeechPercentage>();

        for (float stopPoint : stopPoints) {

            int stopPointInSecond = (int) stopPoint;
            int startPointSplit = (stopPointInSecond / splitSecond) * splitSecond;
            int endPointSplit = (stopPointInSecond / splitSecond + 1) * splitSecond;

            SpeechPercentage sp = new SpeechPercentage();
            float totalSpeechDoctor = 0;
            float totalSpeechPatient = 0;

            if (stopPoint == 0) {
                sp.setTime(stopPoint);
                sp.setDoctorPercentage(0);
                sp.setPatientPercentage(0);
            } else {
                for (EndPoint endPoint : endPointsPatient) {
                    if (endPoint.getStart() >= startPointSplit && endPoint.getEnd() <= endPointSplit) {
                        if (endPoint.getEnd() <= stopPoint) {
                            totalSpeechPatient += (endPoint.getEnd() - endPoint.getStart());
                        } else if (endPoint.getEnd() > stopPoint && endPoint.getStart() <= stopPoint) {
                            totalSpeechPatient += (stopPoint - endPoint.getStart());
                        }
                    }
                }

                for (EndPoint endPoint : endPointsDoctor) {
                    if (endPoint.getStart() >= startPointSplit && endPoint.getEnd() <= endPointSplit) {
                        if (endPoint.getEnd() <= stopPoint) {
                            totalSpeechDoctor += (endPoint.getEnd() - endPoint.getStart());
                        } else if (endPoint.getEnd() > stopPoint && endPoint.getStart() <= stopPoint) {
                            totalSpeechDoctor += (stopPoint - endPoint.getStart());
                        }
                    }
                }
                sp.setTime(stopPoint);
                sp.setDoctorPercentage(totalSpeechDoctor);
                sp.setPatientPercentage(totalSpeechPatient);
            }
            sps.add(sp);
        }

        return sps;

    }


}
