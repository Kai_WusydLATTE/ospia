package org.poscomp.eqclinic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.UnavailablePeriod;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.CalibrationUserService;
import org.poscomp.eqclinic.service.interfaces.UnavailablePeriodService;
import org.poscomp.eqclinic.util.ReliabilityCalculator;
import org.poscomp.survey.domain.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class UnavailablePeriodController {

    private static final Logger logger = Logger.getLogger(UnavailablePeriodController.class);

    @Autowired
    private UnavailablePeriodService unavailablePeriodService;

    @RequestMapping(value = "/survey/saveperiod", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue savePeriod(HttpServletRequest request, @RequestBody UnavailablePeriod unavailablePeriod) {

        ReturnValue rv = new ReturnValue();

        if (unavailablePeriod != null) {
            unavailablePeriodService.saveUnavailablePeriod(unavailablePeriod);
            request.getSession().setAttribute("periods", unavailablePeriod);
            rv.setCode(1);
        } else {
            rv.setCode(0);
        }

        return rv;
    }


    @RequestMapping(value = "/survey/periods", method = {RequestMethod.GET})
    public String getPeriods(HttpServletRequest request) {
        request.setAttribute("redpage", "period_admin");
        request.setAttribute("periods", unavailablePeriodService.getUnavailablePeriod());
        return "survey/listperiod";
    }
    
    
    @RequestMapping(value = "/survey/editperiod/{periodId}")
    @ResponseBody
    public UnavailablePeriod editPeriod(HttpServletRequest request, @PathVariable int periodId) {
        UnavailablePeriod editPeriod = unavailablePeriodService.getUnavailablePeriodById(periodId);
        return editPeriod;
    }
    
    
    @RequestMapping(value = "/survey/deleteperiod/{periodId}")
    @ResponseBody
    public UnavailablePeriod deletePeriod(HttpServletRequest request, @PathVariable int periodId) {
        UnavailablePeriod editPeriod = unavailablePeriodService.getUnavailablePeriodById(periodId);
        if(editPeriod!=null){
            editPeriod.setStatus(0);
            unavailablePeriodService.saveUnavailablePeriod(editPeriod);
            return editPeriod;
        }else{
           return null;
        }
    }
    
}
