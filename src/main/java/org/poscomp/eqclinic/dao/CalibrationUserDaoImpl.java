package org.poscomp.eqclinic.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.CalibrationUserDao;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "calibrationUserDao")
public class CalibrationUserDaoImpl implements CalibrationUserDao {

    private static final Logger logger = Logger.getLogger(CalibrationUserDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveCalibrationUser(CalibrationUser cUser) {
        sessionFactory.getCurrentSession().saveOrUpdate(cUser);
        logger.info("you have insert an calibration user");

    }

    @Override
    public void saveCalibrationSurveyAnswer(CalibrationSurveyAnswer answer) {
        sessionFactory.getCurrentSession().saveOrUpdate(answer);
        logger.info("you have insert an calibration survey");
    }

    @Override
    public CalibrationSurveyAnswer getCalibrationSurveyAnswerById(int id) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from CalibrationSurveyAnswer as ca where ca.id=:id";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        CalibrationSurveyAnswer result = (CalibrationSurveyAnswer) query.uniqueResult();
        return result;
    }

    @Override
    public CalibrationSurveyAnswer getCalibrationSurveyAnswerByUserIdSessionId(int userId, int sessionId) {
        Session session = sessionFactory.getCurrentSession();
        String hql ="from CalibrationSurveyAnswer as ca where  ca.calibrationUserId=:userId and ca.videoId=:sessionId order by ca.createAt desc";
        Query query = session.createQuery(hql);
        query.setInteger("userId", userId);
        query.setInteger("sessionId", sessionId);
        List<CalibrationSurveyAnswer> result = (List<CalibrationSurveyAnswer>) query.list();

        if (result != null && result.size() > 0) {
            return result.get(0);
        }

        return null;
    }


}
