package org.poscomp.eqclinic.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.VideoDao;
import org.poscomp.eqclinic.domain.Video;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Repository(value = "videoDao")
public class VideoDaoImpl implements VideoDao {

    private static final Logger logger = Logger.getLogger(VideoDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveVideo(Video v) {
        sessionFactory.getCurrentSession().saveOrUpdate(v);
        logger.info("you have insert a video");
    }

    @Override
    public Video getVideo(String videoName) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Video as v where v.name=:videoName";
        Query query = session.createQuery(hql);
        query.setString("videoName", videoName);
        Video result = (Video) query.uniqueResult();
        return result;
    }

    @Override
    public List<Video> getVideoByDoctorId(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Video as v where v.doctorId=:doctorId and v.videoType=1 order by v.generateTime desc";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Video> list = (ArrayList<Video>) query.list();
        return list;
    }

    @Override
    public Video getVideoByArchiveId(String archiveId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Video as v where v.archiveId=:archiveId";
        Query query = session.createQuery(hql);
        query.setString("archiveId", archiveId);
        Video result = (Video) query.uniqueResult();
        return result;
    }

    @Override
    public List<Object> getVideoByDoctorIdWithProgress(int doctorId) {
        Session session = sessionFactory.getCurrentSession();
        String hql =
                        "from Video as v, AppointmentProgress as ap, Appointment as apt where v.sessionId = ap.sessionId and v.sessionId = apt.sessionId "
                                        + " and ap.type='doctor' and v.doctorId=:doctorId and v.videoType=1 order by v.generateTime desc";
        Query query = session.createQuery(hql);
        query.setInteger("doctorId", doctorId);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Object> getVideoByPatientIdWithProgress(int patientId) {
        Session session = sessionFactory.getCurrentSession();
        String hql =
                        "from Video as v, AppointmentProgress as ap where v.sessionId = ap.sessionId and ap.type='patient' and "
                                        + " v.patientId=:patientId and v.videoType=1 and ap.hasFinish=0 order by v.generateTime desc";
        Query query = session.createQuery(hql);
        query.setInteger("patientId", patientId);
        List<Object> list = (ArrayList<Object>) query.list();
        return list;
    }

    @Override
    public List<Video> getVideoBySessionId(String sessionId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Video as v where v.sessionId=:sessionId order by v.id desc";
        Query query = session.createQuery(hql);
        query.setString("sessionId", sessionId);
        List<Video> result = (List<Video>) query.list();
        return result;
    }

    @Override
    public List<Video> getVideoByDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date today = calendar.getTime();
        long todayLong = today.getTime() - 48 * 60 * 60 * 1000;
        long tomorrowLong = today.getTime() - 24 * 60 * 60 * 1000;

//        long todayLong = today.getTime();
//        long tomorrowLong = today.getTime() + 24 * 60 * 60 * 1000;

        Session session = sessionFactory.getCurrentSession();
        String hql ="from Video as v, Appointment as a where v.generateTime>:startTime and v.generateTime<:endTime and videoType=1 and v.sessionId = a.sessionId and a.appropriateStu<>0 and a.appropriateSp<>0 ";
        Query query = session.createQuery(hql);
        query.setLong("startTime", todayLong);
        query.setLong("endTime", tomorrowLong);
        List<Object[]> result = (List<Object[]>) query.list();
        List<Video> returnValue = new ArrayList<Video>();

        for (Object[] obj : result) {
            Video v = (Video) obj[0];
            returnValue.add(v);
        }
        return returnValue;
    }

    @Override
    public Video getVideoById(int videoId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Video as v where v.id=:videoId";
        Query query = session.createQuery(hql);
        query.setInteger("videoId", videoId);
        Video result = (Video) query.uniqueResult();
        return result;

    }

    @Override
    public List<Video> getVideoByDate(String dateStr) {

        String pattern = "yyyy-MM-dd HH:mm:ss.S";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date today = format.parse(dateStr + " 00:00:00.0");
            long todayStart = today.getTime();
            long todayEnd = todayStart + 24 * 60 * 60 * 1000;
            System.out.println(todayStart);
            System.out.println(todayEnd);

            Session session = sessionFactory.getCurrentSession();
            String hql =
                            "from Video as v, Appointment as a where v.generateTime>:startTime and v.generateTime<:endTime and videoType=1 and v.sessionId = a.sessionId and a.appropriateStu<>0 and a.appropriateSp<>0 ";
            Query query = session.createQuery(hql);
            query.setLong("startTime", todayStart);
            query.setLong("endTime", todayEnd);
            List<Object[]> result = (List<Object[]>) query.list();
            List<Video> returnValue = new ArrayList<Video>();

            for (Object[] obj : result) {
                Video v = (Video) obj[0];
                returnValue.add(v);
            }
            return returnValue;


        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws ParseException {

        String pattern = "yyyy-MM-dd HH:mm:ss.S";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date date = format.parse("2016-02-16 00:00:00.0");
            System.out.println(date.getTime());
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Video> getAllUnreviewedVideo() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Video as v where v.videoType=1 and (v.reviewComments is null or v.reviewSoca is null or v.reviewBehav is null)";
        Query query = session.createQuery(hql);
        List<Video> result = (List<Video>) query.list();
        return result;
    }

}
