package org.poscomp.eqclinic.processor;

import org.poscomp.eqclinic.util.SoundTool;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SoundProcessor implements Runnable {

    private String audioFileDir;
    private String audioFileName;
    private String saveFileDir;
    private String saveFileName;

    public SoundProcessor(String audioFileDir, String audioFileName, String saveFileDir, String saveFileName) {
        this.audioFileDir = audioFileDir;
        this.audioFileName = audioFileName;
        this.saveFileDir = saveFileDir;
        this.saveFileName = saveFileName;
    }

    public void run() {

        if (audioFileDir.indexOf(".wav") > 0) {
            String[] temp = audioFileDir.split("\\.");
            audioFileDir = temp[0];
        }

        SoundTool tool = new SoundTool();

        if (audioFileName.contains("doctor")) {
            tool.processLoudness(audioFileDir, audioFileName, saveFileDir, saveFileName);
            tool = null;

//            tool = new SoundTool();
//            tool.processPitch(audioFileDir, audioFileName, saveFileDir, saveFileName);
//            tool = null;
            Cmd cmd = new Cmd();
            cmd.processPitchAudio("c:\\videos\\ffmpeg.exe", audioFileDir, audioFileName);
            
            
            tool = new SoundTool();
            tool.processSoundProperty(audioFileDir, audioFileName, saveFileDir, saveFileName);
            tool = null;
        }

        tool = new SoundTool();
        tool.processEndpoint(audioFileDir, audioFileName, saveFileDir, saveFileName, 0);
        tool = null;

        tool = new SoundTool();
        tool.shrinkEndpointFile(saveFileDir + "/" + saveFileName + "_endpoint.txt", saveFileDir + "/" + saveFileName
                        + "_endpoint_new.txt", 0.3f);

    }

}
