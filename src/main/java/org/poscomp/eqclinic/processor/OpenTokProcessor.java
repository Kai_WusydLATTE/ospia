package org.poscomp.eqclinic.processor;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.pojo.MyArchive;

import com.opentok.Archive;
import com.opentok.MediaMode;
import com.opentok.OpenTok;
import com.opentok.SessionProperties;
import com.opentok.exception.OpenTokException;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class OpenTokProcessor {

    private static final Logger logger = Logger.getLogger(OpenTokProcessor.class);

    private static final String apiKey = "45493902"; 
    
    private static final String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
    
    
    public static void main(String[] args) {
        
        OpenTokProcessor op = new OpenTokProcessor();
        for (int i = 0; i < 10; i++) {
            op.generateSessionId();
        }
        
        
    }
    
    public static void tokenGenerator() throws OpenTokException {

        String token = null;
        OpenTok opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        String sessionId = opentok.createSession(new SessionProperties.Builder().mediaMode(MediaMode.ROUTED).build())
                        .getSessionId();
        token = opentok.generateToken(sessionId);

        System.out.println(sessionId);
        System.out.println("token: " + token);
    }

    /*
     * generate a sessionId
     */
    public static String generateSessionId() {

        OpenTok opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        try {
            String sessionId =
                            opentok.createSession(new SessionProperties.Builder().mediaMode(MediaMode.ROUTED).build())
                                            .getSessionId();
            System.out.println("sessionId : "+sessionId);
            
            return sessionId;
        } catch (OpenTokException e) {
            logger.error(e.getMessage(), e);
        }

        return "";
    }

    public static void deleteAllArchives() throws OpenTokException {

        OpenTok opentok;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
        List<Archive> archives = opentok.listArchives(0, 1000);

        for (Archive archive : archives) {
            opentok.deleteArchive(archive.getId());
        }
    }

    public static void listAllArchives() throws OpenTokException {

        OpenTok opentok;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);

        List<Archive> archives = opentok.listArchives(0, 1000);

        for (Archive archive : archives) {
            if (archive.getDuration() != 0 && archive.getUrl() != null && !"".equals(archive.getUrl())) {
                MyArchive mr = new MyArchive();
                mr.setCreateTime(archive.getCreatedAt());
                mr.setLength(archive.getDuration());
                mr.setUrl(archive.getUrl());
                System.out.println(archive.getDuration());
                System.out.println(archive.getUrl());
            }
        }
    }

    public static void getArchives(String name) throws OpenTokException {

        OpenTok opentok;
        opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);

        Archive archive = opentok.getArchive(name);

        System.out.println(archive.getDuration());
        System.out.println(archive.getUrl());

    }

}
