package org.poscomp.eqclinic.test;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class CalendarTest {

    // public static void main(String[] args) {
    // CalendarTest ct = new CalendarTest();
    // ct.getEventDate();
    //
    // }

    public static void addEvent() {

        final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
        final JsonFactory JSON_FACTORY = new JacksonFactory();

        String team1_email = "q3794e2o24i7pg8ellu2fsg93s@group.calendar.google.com";
        String team2_email = "21pkcuafquuamdubf655e4t5lg@group.calendar.google.com";
        String team3_email = "9u6c2jguttkr7l02brf18sbusk@group.calendar.google.com";

        Collection<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);
        File licenseFile = new File("qingscal-9e10a784e990.p12");

        GoogleCredential credential;
        try {
            credential =
                            new GoogleCredential.Builder()
                                            .setTransport(HTTP_TRANSPORT)
                                            .setJsonFactory(JSON_FACTORY)
                                            .setServiceAccountId(
                                                            "1039242859127-jvdeilpms8kp84lpsfo5n5jdm9kdrr1e@developer.gserviceaccount.com")
                                            .setServiceAccountScopes(SCOPES)
                                            .setServiceAccountPrivateKeyFromP12File(licenseFile).build();

            Calendar service =
                            new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
                                            "Google Calendar Sync").build();
            Event event = new Event().setSummary("Appointment from chunfeng liu").setDescription("scenarios AAA #123");

            DateTime startDateTime = new DateTime("2015-07-25T09:00:00+10:00");
            EventDateTime start = new EventDateTime().setDateTime(startDateTime).setTimeZone("Australia/Sydney");
            event.setStart(start);

            DateTime endDateTime = new DateTime("2015-07-25T09:30:00+10:00");
            EventDateTime end = new EventDateTime().setDateTime(endDateTime).setTimeZone("Australia/Sydney");
            event.setEnd(end);

            // String[] recurrence = new String[] {"RRULE:FREQ=DAILY;COUNT=2"};
            // event.setRecurrence(Arrays.asList(recurrence));
            event = service.events().insert(team3_email, event).execute();

            System.out.println(event.toString());

        } catch (GeneralSecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getEventDate() {

        final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
        final JsonFactory JSON_FACTORY = new JacksonFactory();

        String team1_email = "q3794e2o24i7pg8ellu2fsg93s@group.calendar.google.com";
        String team2_email = "21pkcuafquuamdubf655e4t5lg@group.calendar.google.com";
        String team3_email = "9u6c2jguttkr7l02brf18sbusk@group.calendar.google.com";

        Collection<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);
        File licenseFile = new File("qingscal-9e10a784e990.p12");

        DateTime startDateTime = new DateTime("2015-07-25T00:00:00+10:00");
        EventDateTime start = new EventDateTime().setDateTime(startDateTime).setTimeZone("Australia/Sydney");

        DateTime endDateTime = new DateTime("2015-07-25T23:59:59+10:00");
        EventDateTime end = new EventDateTime().setDateTime(endDateTime).setTimeZone("Australia/Sydney");

        GoogleCredential credential;
        try {
            credential =
                            new GoogleCredential.Builder()
                                            .setTransport(HTTP_TRANSPORT)
                                            .setJsonFactory(JSON_FACTORY)
                                            .setServiceAccountId(
                                                            "1039242859127-jvdeilpms8kp84lpsfo5n5jdm9kdrr1e@developer.gserviceaccount.com")
                                            .setServiceAccountScopes(SCOPES)
                                            .setServiceAccountPrivateKeyFromP12File(licenseFile).build();

            Calendar service =
                            new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
                                            "Google Calendar Sync").build();
            // Iterate over the events in the specified calendar
            String pageToken = null;
            do {
                Events events =
                                service.events().list(team3_email).setTimeMin(startDateTime).setTimeMax(endDateTime)
                                                .setPageToken(pageToken).execute();
                List<Event> items = events.getItems();
                for (Event event : items) {
                    System.out.println(event.getSummary());
                }
                pageToken = events.getNextPageToken();
            } while (pageToken != null);

        } catch (GeneralSecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void getCals() throws IOException, InterruptedException, GeneralSecurityException {

        final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
        final JsonFactory JSON_FACTORY = new JacksonFactory();

        String team1_email = "q3794e2o24i7pg8ellu2fsg93s@group.calendar.google.com";
        String team2_email = "21pkcuafquuamdubf655e4t5lg@group.calendar.google.com";
        String team3_email = "9u6c2jguttkr7l02brf18sbusk@group.calendar.google.com";

        Collection<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);
        File licenseFile = new File("qingscal-9e10a784e990.p12");

        GoogleCredential credential =
                        new GoogleCredential.Builder()
                                        .setTransport(HTTP_TRANSPORT)
                                        .setJsonFactory(JSON_FACTORY)
                                        .setServiceAccountId(
                                                        "1039242859127-jvdeilpms8kp84lpsfo5n5jdm9kdrr1e@developer.gserviceaccount.com")
                                        .setServiceAccountScopes(SCOPES)
                                        .setServiceAccountPrivateKeyFromP12File(licenseFile).build();

        Calendar service =
                        new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
                                        "Google Calendar Sync").build();
        // Iterate over the events in the specified calendar
        String pageToken = null;
        do {
            Events events = service.events().list(team3_email).setPageToken(pageToken).execute();
            List<Event> items = events.getItems();
            for (Event event : items) {
                System.out.println(event.getSummary());
            }
            pageToken = events.getNextPageToken();
        } while (pageToken != null);

    }
}
