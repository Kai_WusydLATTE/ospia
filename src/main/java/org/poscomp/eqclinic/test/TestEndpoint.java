package org.poscomp.eqclinic.test;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

import endpoint.EndPoint;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class TestEndpoint {

    public static void main(String[] args) {

        Object[] result = null;
        EndPoint endp = null;

        try {
            endp = new EndPoint();
            long curtime = System.currentTimeMillis();
            result = endp.endpoint(1, "C:/Program Files/apache-tomcat-7.0.55/webapps/ospia/video/video_1455346347759/video_1455169829362_doctor_baseline.wav");
            System.out.println(System.currentTimeMillis()-curtime);
            MWNumericArray temp = (MWNumericArray) result[0];
            float[][] weights = (float[][]) temp.toFloatArray();

            System.out.println(weights.length);
            endp.dispose();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            MWArray.disposeArray(result);
        }

    }

}
