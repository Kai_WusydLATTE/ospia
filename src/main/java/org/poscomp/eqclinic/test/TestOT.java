package org.poscomp.eqclinic.test;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.opentok.Archive;
import com.opentok.MediaMode;
import com.opentok.OpenTok;
import com.opentok.SessionProperties;
import com.opentok.exception.OpenTokException;

public class TestOT {

	public static void main(String[] args) throws OpenTokException, UnsupportedEncodingException {
  
//		TestOT.tokenGenerator();
		listAllArchives();
//		deleteAllArchives();
		
//		for (int i = 0; i < 5; i++) {
//			tokenGenerator();	
//		}
	}

	public static void tokenGenerator() throws OpenTokException {
		String apiKey = "45493902";
		String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";

//		String token = null;
		OpenTok opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
		String sessionId = opentok.createSession(new SessionProperties.Builder().mediaMode(MediaMode.ROUTED).build()).getSessionId();
//		token = opentok.generateToken(sessionId);
		
		System.out.println(sessionId);
//		System.out.println("token: "+token);
	}

	
	// generate a sessionId
	public static void generateSessionId(){
		// method of create a session
//		sessionId = opentok.createSession(
//				new SessionProperties.Builder().mediaMode(MediaMode.ROUTED)
//						.build()).getSessionId();
	}

	public static void deleteAllArchives() throws OpenTokException {
		String apiKey = "45493902";
		String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
		OpenTok opentok;
		opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
		List<Archive> archives = opentok.listArchives(0, 1000);

		for (Archive archive : archives) {
			opentok.deleteArchive(archive.getId());
		}
	}

	public static void listAllArchives() throws OpenTokException {
		String apiKey = "45493902";
		String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
		OpenTok opentok;
		opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
		
		List<Archive> archives = opentok.listArchives(0, 1000);
		
		for (Archive archive : archives) {
			if(archive.getDuration() != 0 && archive.getUrl() !=null && !"".equals(archive.getUrl())){
			    System.out.println(archive.getId());
			    System.out.println(archive.getDuration());
				System.out.println(archive.getUrl());
			}			
		}
		
	}

	
	public static void getArchives(String name) throws OpenTokException {
		String apiKey = "45493902";
		String apiSecret = "54d8c8370b3828f633154dc04810e3af4535646f";
		OpenTok opentok;
		opentok = new OpenTok(Integer.parseInt(apiKey), apiSecret);
		
		Archive archive = opentok.getArchive(name);
		
		System.out.println(archive.getDuration());
		System.out.println(archive.getUrl());
	}
	
}
