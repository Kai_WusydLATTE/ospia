package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.ReflectionLog;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface ReflectionLogService {
    
    public void save(ReflectionLog reflectionLog);
    
    public ReflectionLog getById(int reflectionLogId);

}
