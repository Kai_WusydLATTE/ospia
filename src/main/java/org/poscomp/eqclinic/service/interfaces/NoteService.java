package org.poscomp.eqclinic.service.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Patient;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public interface NoteService {

    public void saveNote(Note note);

    public int saveNotePatient(Note note, Appointment apt, Patient patient);

    public int saveNoteTutor(Note note);
    
    public void deleteNote(Note note);
    
    public Note getNoteById(int noteId);

    public List<Note> getNoteBySessionId(String sessionId);

    public List<Note> getNoteBySessionIdNoter(String sessionId, int noterType, int noterId);
    
    public List<Note> getNoteBySessionIdType(String sessionId, int noterType, int noteType);
    
    public List<Note> getNoteBySessionIdTypeAll(String sessionId, int noterType);

}
