package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.LoginLogDao;
import org.poscomp.eqclinic.domain.LoginLog;
import org.poscomp.eqclinic.service.interfaces.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "loginLogService")
public class LoginLogServiceImpl implements LoginLogService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private LoginLogDao loginLogDao;

    @Override
    public void saveLoginLog(LoginLog loginLog) {
        loginLogDao.saveLoginLog(loginLog);
    }

}
