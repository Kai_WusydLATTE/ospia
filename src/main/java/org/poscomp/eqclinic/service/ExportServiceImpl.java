package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.ExportDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.service.interfaces.ApplicationService;
import org.poscomp.eqclinic.service.interfaces.ExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "exportService")
public class ExportServiceImpl implements ExportService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private ExportDao exportDao;

    @Override
    public List<Object> exportSPRegistration(long timestamp) {
        return exportDao.exportSPRegistration(timestamp);
    }

    @Override
    public List<Object> exportAptOffered(long timestamp) {
        return exportDao.exportAptOffered(timestamp);
    }

    @Override
    public List<Object> exportAptCompleted(long timestamp) {
        return exportDao.exportAptCompleted(timestamp);
    }

    @Override
    public List<Object> exportAptFailed(long timestamp) {
        return exportDao.exportAptFailed(timestamp);
    }

    @Override
    public List<Object> exportAptCanceledStu(long timestamp) {
        return exportDao.exportAptCanceledStu(timestamp);
    }
    
    @Override
    public List<Object> exportAptCanceledSP(long timestamp) {
        return exportDao.exportAptCanceledSP(timestamp);
    }

    @Override
    public List<Object> exportAptCompletedSP(long timestamp) {
        return exportDao.exportAptCompletedSP(timestamp);
    }

    @Override
    public List<Object> exportAptCompletedStu(long timestamp) {
        return exportDao.exportAptCompletedStu(timestamp);
    }

    @Override
    public List<Object> exportAptImappropriate(long timestamp) {
        return exportDao.exportAptImappropriate(timestamp);
    }

    @Override
    public List<Object> exportAptConclusion(int status, long timestamp) {
        return exportDao.exportAptConclusion(status, timestamp);
    }

}
