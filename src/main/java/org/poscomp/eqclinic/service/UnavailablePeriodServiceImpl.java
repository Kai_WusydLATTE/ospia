package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.UnavailablePeriodDao;
import org.poscomp.eqclinic.domain.UnavailablePeriod;
import org.poscomp.eqclinic.service.interfaces.UnavailablePeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "unavailablePeriodService")
public class UnavailablePeriodServiceImpl implements UnavailablePeriodService {

    private static final Logger logger = Logger.getLogger(UnavailablePeriodServiceImpl.class);

    @Autowired
    private UnavailablePeriodDao unavailablePeriodDao;

    @Override
    public void saveUnavailablePeriod(UnavailablePeriod period) {
        unavailablePeriodDao.saveUnavailablePeriod(period);
    }

    @Override
    public UnavailablePeriod getUnavailablePeriodById(int id) {
        return unavailablePeriodDao.getUnavailablePeriodById(id);
    }

    @Override
    public List<UnavailablePeriod> getUnavailablePeriod() {
        return unavailablePeriodDao.getUnavailablePeriod();
    }

}
