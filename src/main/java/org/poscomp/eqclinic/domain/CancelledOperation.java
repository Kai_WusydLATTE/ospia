package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cancelledOperation", catalog = "ospia")
public class CancelledOperation implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer aptId;
    private Integer doctorId;
    private Integer patientId;
    private String reason;
    private Long cancelAt;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "aptId")
    public Integer getAptId() {
        return aptId;
    }

    public void setAptId(Integer aptId) {
        this.aptId = aptId;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "patientId")
    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    @Column(name = "reason", length = 3000)
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Column(name = "cancelAt")
    public Long getCancelAt() {
        return cancelAt;
    }

    public void setCancelAt(Long cancelAt) {
        this.cancelAt = cancelAt;
    }
}
