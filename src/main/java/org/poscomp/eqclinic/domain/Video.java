package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "video", catalog = "ospia")
public class Video implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private Long generateTime;
    private String url;
    private Integer length;
    private String location;
    private Integer state;
    private String archiveId;
    private Integer doctorId;
    private Integer patientId;
    private String patientName;
    private String doctorName;
    private String generateTimeStr;
    private String sessionId;
    private String aptDate;
    private String aptTime;
    private String sceCode;
    // 1: combine video 2: doctor self video 3: patient self video
    private Integer videoType;
    // indicate whether tutors comments could be viewed by the doctor
    private Integer allowTutorNotes;

    private Integer reviewComments;
    private Integer reviewSoca;
    private Integer reviewBehav;
    private Integer reviewSurvey;
    private Integer reviewTutorSoca;
    
    // indicate whether allow give feedback for student
    private Integer allowFeedback;
    
    // Levels: 0: no email 1: normal email 2: email with result 3: email with classmates
    private Integer emailLevel;
    
    // (at this email level) 0: has not send an email 1: has send an email
    private Integer hasSendEmail;
    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "generateTime")
    public Long getGenerateTime() {
        return generateTime;
    }

    public void setGenerateTime(Long generateTime) {
        this.generateTime = generateTime;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "length")
    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "state")
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Column(name = "archiveId")
    public String getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(String archiveId) {
        this.archiveId = archiveId;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "patientId")
    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    @Column(name = "generateTimeStr")
    public String getGenerateTimeStr() {
        return generateTimeStr;
    }

    public void setGenerateTimeStr(String generateTimeStr) {
        this.generateTimeStr = generateTimeStr;
    }

    @Column(name = "patientName")
    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    @Column(name = "sessionId")
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "videoType")
    public Integer getVideoType() {
        return videoType;
    }

    public void setVideoType(Integer videoType) {
        this.videoType = videoType;
    }

    @Column(name = "aptDate")
    public String getAptDate() {
        return aptDate;
    }

    public void setAptDate(String aptDate) {
        this.aptDate = aptDate;
    }

    @Column(name = "aptTime")
    public String getAptTime() {
        return aptTime;
    }

    public void setAptTime(String aptTime) {
        this.aptTime = aptTime;
    }

    @Column(name = "sceCode")
    public String getSceCode() {
        return sceCode;
    }

    public void setSceCode(String sceCode) {
        this.sceCode = sceCode;
    }

    @Column(name = "doctorName")
    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    @Column(name = "allowTutorNotes")
    public Integer getAllowTutorNotes() {
        return allowTutorNotes;
    }

    public void setAllowTutorNotes(Integer allowTutorNotes) {
        this.allowTutorNotes = allowTutorNotes;
    }

    @Column(name = "reviewComments")
    public Integer getReviewComments() {
        return reviewComments;
    }

    public void setReviewComments(Integer reviewComments) {
        this.reviewComments = reviewComments;
    }

    @Column(name = "reviewSoca")
    public Integer getReviewSoca() {
        return reviewSoca;
    }

    public void setReviewSoca(Integer reviewSoca) {
        this.reviewSoca = reviewSoca;
    }

    @Column(name = "reviewBehav")
    public Integer getReviewBehav() {
        return reviewBehav;
    }

    public void setReviewBehav(Integer reviewBehav) {
        this.reviewBehav = reviewBehav;
    }

    @Column(name = "reviewSurvey")
    public Integer getReviewSurvey() {
        return reviewSurvey;
    }

    public void setReviewSurvey(Integer reviewSurvey) {
        this.reviewSurvey = reviewSurvey;
    }

    @Column(name = "allowFeedback")
    public Integer getAllowFeedback() {
        return allowFeedback;
    }

    public void setAllowFeedback(Integer allowFeedback) {
        this.allowFeedback = allowFeedback;
    }

    @Column(name = "reviewTutorSoca")
    public Integer getReviewTutorSoca() {
        return reviewTutorSoca;
    }

    public void setReviewTutorSoca(Integer reviewTutorSoca) {
        this.reviewTutorSoca = reviewTutorSoca;
    }

    @Column(name = "emailLevel")
    public Integer getEmailLevel() {
        return emailLevel;
    }

    public void setEmailLevel(Integer emailLevel) {
        this.emailLevel = emailLevel;
    }

    @Column(name = "hasSendEmail")
    public Integer getHasSendEmail() {
        return hasSendEmail;
    }

    public void setHasSendEmail(Integer hasSendEmail) {
        this.hasSendEmail = hasSendEmail;
    }
    
}
