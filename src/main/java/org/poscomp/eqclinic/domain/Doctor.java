package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "doctor", catalog = "ospia")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer doctorId;
    private String firstname;
    private String lastname;
    private Integer status;
    private String username;
    private String password;
    private Long lastLoginAt;
    // for students, this is the SID
    private String userId;
    // whether sign the agreement
    private Integer agreement;
    private Integer training;
    private Integer year;
    private Integer phase;
    
    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "doctorId", unique = true, nullable = false)
    public Integer getDoctorId() {
        return this.doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "firstname", length=300)
    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "lastname", length=300)
    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column(name = "status")
    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "username", length=300)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length=300)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "lastLoginAt")
    public Long getLastLoginAt() {
        return lastLoginAt;
    }

    public void setLastLoginAt(Long lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    @Column(name = "userId", length=300)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "agreement")
    public Integer getAgreement() {
        return agreement;
    }

    public void setAgreement(Integer agreement) {
        this.agreement = agreement;
    }

    @Column(name = "training")
    public Integer getTraining() {
        return training;
    }

    public void setTraining(Integer training) {
        this.training = training;
    }

    @Column(name = "year")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Column(name = "phase")
    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

}
