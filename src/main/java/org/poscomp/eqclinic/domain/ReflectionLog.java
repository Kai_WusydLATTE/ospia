package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reflectionlog", catalog = "ospia")
public class ReflectionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer logId;

    private String sessionId;

    private Integer doctorId;

    private String nonverbalType;

    private Long startTime;

    private Long endTime;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "logId", unique = true, nullable = false)
    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    @Column(name = "sessionId", length = 300)
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "nonverbalType")
    public String getNonverbalType() {
        return nonverbalType;
    }

    public void setNonverbalType(String nonverbalType) {
        this.nonverbalType = nonverbalType;
    }

    @Column(name = "startTime")
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Column(name = "endTime")
    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

}
