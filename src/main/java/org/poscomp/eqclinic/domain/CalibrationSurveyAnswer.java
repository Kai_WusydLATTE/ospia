package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "calibrationSurveyAnswer", catalog = "ospia")
public class CalibrationSurveyAnswer implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer videoId;
    private Integer calibrationUserId;
    private String surveyAnswer;
    private Long createAt;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "videoId")
    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    @Column(name = "calibrationUserId")
    public Integer getCalibrationUserId() {
        return calibrationUserId;
    }

    public void setCalibrationUserId(Integer calibrationUserId) {
        this.calibrationUserId = calibrationUserId;
    }

    @Column(name = "surveyAnswer", length = 10000)
    public String getSurveyAnswer() {
        return surveyAnswer;
    }

    public void setSurveyAnswer(String surveyAnswer) {
        this.surveyAnswer = surveyAnswer;
    }

    @Column(name = "createAt")
    public Long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Long createAt) {
        this.createAt = createAt;
    }

}
