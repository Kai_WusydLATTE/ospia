package org.poscomp.eqclinic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "emailClicking", catalog = "ospia")
public class EmailClicking {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer doctorId;
    private Integer emailType;
    private Integer emailLevel;
    private Long clickAt;

    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "emailType")
    public Integer getEmailType() {
        return emailType;
    }

    public void setEmailType(Integer emailType) {
        this.emailType = emailType;
    }

    @Column(name = "emailLevel")
    public Integer getEmailLevel() {
        return emailLevel;
    }

    public void setEmailLevel(Integer emailLevel) {
        this.emailLevel = emailLevel;
    }

    @Column(name = "clickAt")
    public Long getClickAt() {
        return clickAt;
    }

    public void setClickAt(Long clickAt) {
        this.clickAt = clickAt;
    }

}
