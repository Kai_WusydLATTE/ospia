package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class FrownTag implements Serializable {

    private static final long serialVersionUID = 1L;

    private int tagId;
    private float value;
    private float time;

    public FrownTag(int tagId, float value, float time) {
        this.tagId = tagId;
        this.value = value;
        this.time = time;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

}
