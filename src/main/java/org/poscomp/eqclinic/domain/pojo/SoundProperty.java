package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SoundProperty implements Serializable {

    private static final long serialVersionUID = 1L;

    private float totalLength;
    private int totalFrame;

    public float getTotalLength() {
        return totalLength;
    }

    public void setTotalLength(float totalLength) {
        this.totalLength = totalLength;
    }

    public int getTotalFrame() {
        return totalFrame;
    }

    public void setTotalFrame(int totalFrame) {
        this.totalFrame = totalFrame;
    }

}
