package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class TurnTakingInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private float propotion;
    private int numberOfTurn;
    private float averageLength;
    private float logestTurn;
    private float logestTurnStartTime;
    private float shortestTurn;
    private float averageResponeTime;
    private float overlappingTime;
    private float overlappingPercent;

    public float getPropotion() {
        return propotion;
    }

    public void setPropotion(float propotion) {
        this.propotion = propotion;
    }

    public int getNumberOfTurn() {
        return numberOfTurn;
    }

    public void setNumberOfTurn(int numberOfTurn) {
        this.numberOfTurn = numberOfTurn;
    }

    public float getAverageLength() {
        return averageLength;
    }

    public void setAverageLength(float averageLength) {
        this.averageLength = averageLength;
    }

    public float getLogestTurn() {
        return logestTurn;
    }

    public void setLogestTurn(float logestTurn) {
        this.logestTurn = logestTurn;
    }

    public float getShortestTurn() {
        return shortestTurn;
    }

    public void setShortestTurn(float shortestTurn) {
        this.shortestTurn = shortestTurn;
    }

    public float getAverageResponeTime() {
        return averageResponeTime;
    }

    public void setAverageResponeTime(float averageResponeTime) {
        this.averageResponeTime = averageResponeTime;
    }

    public float getOverlappingTime() {
        return overlappingTime;
    }

    public void setOverlappingTime(float overlappingTime) {
        this.overlappingTime = overlappingTime;
    }

    public float getOverlappingPercent() {
        return overlappingPercent;
    }

    public void setOverlappingPercent(float overlappingPercent) {
        this.overlappingPercent = overlappingPercent;
    }

    public float getLogestTurnStartTime() {
        return logestTurnStartTime;
    }

    public void setLogestTurnStartTime(float logestTurnStartTime) {
        this.logestTurnStartTime = logestTurnStartTime;
    }
}
