package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class TouchObj implements Comparable<TouchObj>, Serializable {

    private static final long serialVersionUID = 1L;

    private int lineNo;
    private double persentage;

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public double getPersentage() {
        return persentage;
    }

    public void setPersentage(double persentage) {
        this.persentage = persentage;
    }

    @Override
    public String toString() {
        return lineNo + ":" + persentage;
    }

    @Override
    public int compareTo(TouchObj other) {
        if (this.persentage < other.getPersentage()) {
            return 1;
        } else if (this.persentage > other.getPersentage()) {
            return -1;
        }
        return 0;
    }

}
