package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "scenario_patient", catalog = "ospia")
public class ScenarioPatient implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private int scenarioId;
    private int patientId;
    private long applyat;
    private long confirmat;
    private int status;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Column(name = "scenarioId")
    public int getScenarioId() {
        return scenarioId;
    }

    public void setScenarioId(int scenarioId) {
        this.scenarioId = scenarioId;
    }

    @Column(name = "patientId")
    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Column(name = "applyat")
    public long getApplyat() {
        return applyat;
    }

    public void setApplyat(long applyat) {
        this.applyat = applyat;
    }

    @Column(name = "confirmat")
    public long getConfirmat() {
        return confirmat;
    }

    public void setConfirmat(long confirmat) {
        this.confirmat = confirmat;
    }

}
