/**
 * Assessment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class Assessment  extends org.poscomp.eqclinic.ws.AssessmentTemplate  implements java.io.Serializable {
    private java.lang.String studentId;

    private java.lang.String assessorId;

    private java.lang.String status;

    private java.lang.String receiptNumber;

    private java.lang.String templateUniqueKey;

    private java.lang.String submissionDate;

    private java.lang.String dateModified;

    private java.lang.String dateStatusModified;

    private org.poscomp.eqclinic.ws.Criterion[] criterionList;

    private org.poscomp.eqclinic.ws.CriterionGrade[] gradedCriterionList;

    public Assessment() {
    }

    public Assessment(
           java.lang.String uniqueKey,
           java.lang.String assessmentType,
           java.lang.String assessmentTitle,
           java.lang.String assessmentName,
           java.lang.String assessmentPhase,
           java.lang.String assessmentCourse,
           boolean userCanCreate,
           org.poscomp.eqclinic.ws.TemplateField assessmentDetail,
           org.poscomp.eqclinic.ws.StudentDetails studentDetails,
           org.poscomp.eqclinic.ws.AssessorDetails assessorDetails,
           org.poscomp.eqclinic.ws.Section[] sectionList,
           org.poscomp.eqclinic.ws.CaseDescription caseDescription,
           org.poscomp.eqclinic.ws.Grading grading,
           org.poscomp.eqclinic.ws.Feedback[] feedbackList,
           org.poscomp.eqclinic.ws.Information[] informationList,
           boolean showTemplate,
           boolean allowWithdraw,
           boolean reviewWorkflow,
           java.lang.String studentId,
           java.lang.String assessorId,
           java.lang.String status,
           java.lang.String receiptNumber,
           java.lang.String templateUniqueKey,
           java.lang.String submissionDate,
           java.lang.String dateModified,
           java.lang.String dateStatusModified,
           org.poscomp.eqclinic.ws.Criterion[] criterionList,
           org.poscomp.eqclinic.ws.CriterionGrade[] gradedCriterionList) {
        super(
            uniqueKey,
            assessmentType,
            assessmentTitle,
            assessmentName,
            assessmentPhase,
            assessmentCourse,
            userCanCreate,
            assessmentDetail,
            studentDetails,
            assessorDetails,
            sectionList,
            caseDescription,
            grading,
            feedbackList,
            informationList,
            showTemplate,
            allowWithdraw,
            reviewWorkflow);
        this.studentId = studentId;
        this.assessorId = assessorId;
        this.status = status;
        this.receiptNumber = receiptNumber;
        this.templateUniqueKey = templateUniqueKey;
        this.submissionDate = submissionDate;
        this.dateModified = dateModified;
        this.dateStatusModified = dateStatusModified;
        this.criterionList = criterionList;
        this.gradedCriterionList = gradedCriterionList;
    }


    /**
     * Gets the studentId value for this Assessment.
     * 
     * @return studentId
     */
    public java.lang.String getStudentId() {
        return studentId;
    }


    /**
     * Sets the studentId value for this Assessment.
     * 
     * @param studentId
     */
    public void setStudentId(java.lang.String studentId) {
        this.studentId = studentId;
    }


    /**
     * Gets the assessorId value for this Assessment.
     * 
     * @return assessorId
     */
    public java.lang.String getAssessorId() {
        return assessorId;
    }


    /**
     * Sets the assessorId value for this Assessment.
     * 
     * @param assessorId
     */
    public void setAssessorId(java.lang.String assessorId) {
        this.assessorId = assessorId;
    }


    /**
     * Gets the status value for this Assessment.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Assessment.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the receiptNumber value for this Assessment.
     * 
     * @return receiptNumber
     */
    public java.lang.String getReceiptNumber() {
        return receiptNumber;
    }


    /**
     * Sets the receiptNumber value for this Assessment.
     * 
     * @param receiptNumber
     */
    public void setReceiptNumber(java.lang.String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }


    /**
     * Gets the templateUniqueKey value for this Assessment.
     * 
     * @return templateUniqueKey
     */
    public java.lang.String getTemplateUniqueKey() {
        return templateUniqueKey;
    }


    /**
     * Sets the templateUniqueKey value for this Assessment.
     * 
     * @param templateUniqueKey
     */
    public void setTemplateUniqueKey(java.lang.String templateUniqueKey) {
        this.templateUniqueKey = templateUniqueKey;
    }


    /**
     * Gets the submissionDate value for this Assessment.
     * 
     * @return submissionDate
     */
    public java.lang.String getSubmissionDate() {
        return submissionDate;
    }


    /**
     * Sets the submissionDate value for this Assessment.
     * 
     * @param submissionDate
     */
    public void setSubmissionDate(java.lang.String submissionDate) {
        this.submissionDate = submissionDate;
    }


    /**
     * Gets the dateModified value for this Assessment.
     * 
     * @return dateModified
     */
    public java.lang.String getDateModified() {
        return dateModified;
    }


    /**
     * Sets the dateModified value for this Assessment.
     * 
     * @param dateModified
     */
    public void setDateModified(java.lang.String dateModified) {
        this.dateModified = dateModified;
    }


    /**
     * Gets the dateStatusModified value for this Assessment.
     * 
     * @return dateStatusModified
     */
    public java.lang.String getDateStatusModified() {
        return dateStatusModified;
    }


    /**
     * Sets the dateStatusModified value for this Assessment.
     * 
     * @param dateStatusModified
     */
    public void setDateStatusModified(java.lang.String dateStatusModified) {
        this.dateStatusModified = dateStatusModified;
    }


    /**
     * Gets the criterionList value for this Assessment.
     * 
     * @return criterionList
     */
    public org.poscomp.eqclinic.ws.Criterion[] getCriterionList() {
        return criterionList;
    }


    /**
     * Sets the criterionList value for this Assessment.
     * 
     * @param criterionList
     */
    public void setCriterionList(org.poscomp.eqclinic.ws.Criterion[] criterionList) {
        this.criterionList = criterionList;
    }

    public org.poscomp.eqclinic.ws.Criterion getCriterionList(int i) {
        return this.criterionList[i];
    }

    public void setCriterionList(int i, org.poscomp.eqclinic.ws.Criterion _value) {
        this.criterionList[i] = _value;
    }


    /**
     * Gets the gradedCriterionList value for this Assessment.
     * 
     * @return gradedCriterionList
     */
    public org.poscomp.eqclinic.ws.CriterionGrade[] getGradedCriterionList() {
        return gradedCriterionList;
    }


    /**
     * Sets the gradedCriterionList value for this Assessment.
     * 
     * @param gradedCriterionList
     */
    public void setGradedCriterionList(org.poscomp.eqclinic.ws.CriterionGrade[] gradedCriterionList) {
        this.gradedCriterionList = gradedCriterionList;
    }

    public org.poscomp.eqclinic.ws.CriterionGrade getGradedCriterionList(int i) {
        return this.gradedCriterionList[i];
    }

    public void setGradedCriterionList(int i, org.poscomp.eqclinic.ws.CriterionGrade _value) {
        this.gradedCriterionList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Assessment)) return false;
        Assessment other = (Assessment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.studentId==null && other.getStudentId()==null) || 
             (this.studentId!=null &&
              this.studentId.equals(other.getStudentId()))) &&
            ((this.assessorId==null && other.getAssessorId()==null) || 
             (this.assessorId!=null &&
              this.assessorId.equals(other.getAssessorId()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.receiptNumber==null && other.getReceiptNumber()==null) || 
             (this.receiptNumber!=null &&
              this.receiptNumber.equals(other.getReceiptNumber()))) &&
            ((this.templateUniqueKey==null && other.getTemplateUniqueKey()==null) || 
             (this.templateUniqueKey!=null &&
              this.templateUniqueKey.equals(other.getTemplateUniqueKey()))) &&
            ((this.submissionDate==null && other.getSubmissionDate()==null) || 
             (this.submissionDate!=null &&
              this.submissionDate.equals(other.getSubmissionDate()))) &&
            ((this.dateModified==null && other.getDateModified()==null) || 
             (this.dateModified!=null &&
              this.dateModified.equals(other.getDateModified()))) &&
            ((this.dateStatusModified==null && other.getDateStatusModified()==null) || 
             (this.dateStatusModified!=null &&
              this.dateStatusModified.equals(other.getDateStatusModified()))) &&
            ((this.criterionList==null && other.getCriterionList()==null) || 
             (this.criterionList!=null &&
              java.util.Arrays.equals(this.criterionList, other.getCriterionList()))) &&
            ((this.gradedCriterionList==null && other.getGradedCriterionList()==null) || 
             (this.gradedCriterionList!=null &&
              java.util.Arrays.equals(this.gradedCriterionList, other.getGradedCriterionList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStudentId() != null) {
            _hashCode += getStudentId().hashCode();
        }
        if (getAssessorId() != null) {
            _hashCode += getAssessorId().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getReceiptNumber() != null) {
            _hashCode += getReceiptNumber().hashCode();
        }
        if (getTemplateUniqueKey() != null) {
            _hashCode += getTemplateUniqueKey().hashCode();
        }
        if (getSubmissionDate() != null) {
            _hashCode += getSubmissionDate().hashCode();
        }
        if (getDateModified() != null) {
            _hashCode += getDateModified().hashCode();
        }
        if (getDateStatusModified() != null) {
            _hashCode += getDateStatusModified().hashCode();
        }
        if (getCriterionList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCriterionList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCriterionList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGradedCriterionList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGradedCriterionList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGradedCriterionList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Assessment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Assessment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("studentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StudentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AssessorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiptNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReceiptNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("templateUniqueKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TemplateUniqueKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submissionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SubmissionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateModified");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DateModified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateStatusModified");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DateStatusModified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criterionList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CriterionList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Criterion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gradedCriterionList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GradedCriterionList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "CriterionGrade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
