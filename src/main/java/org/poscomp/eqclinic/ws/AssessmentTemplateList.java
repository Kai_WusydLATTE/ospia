/**
 * AssessmentTemplateList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class AssessmentTemplateList  implements java.io.Serializable {
    private org.poscomp.eqclinic.ws.AssessmentTemplate[] assessmentList;

    private short count;

    public AssessmentTemplateList() {
    }

    public AssessmentTemplateList(
           org.poscomp.eqclinic.ws.AssessmentTemplate[] assessmentList,
           short count) {
           this.assessmentList = assessmentList;
           this.count = count;
    }


    /**
     * Gets the assessmentList value for this AssessmentTemplateList.
     * 
     * @return assessmentList
     */
    public org.poscomp.eqclinic.ws.AssessmentTemplate[] getAssessmentList() {
        return assessmentList;
    }


    /**
     * Sets the assessmentList value for this AssessmentTemplateList.
     * 
     * @param assessmentList
     */
    public void setAssessmentList(org.poscomp.eqclinic.ws.AssessmentTemplate[] assessmentList) {
        this.assessmentList = assessmentList;
    }

    public org.poscomp.eqclinic.ws.AssessmentTemplate getAssessmentList(int i) {
        return this.assessmentList[i];
    }

    public void setAssessmentList(int i, org.poscomp.eqclinic.ws.AssessmentTemplate _value) {
        this.assessmentList[i] = _value;
    }


    /**
     * Gets the count value for this AssessmentTemplateList.
     * 
     * @return count
     */
    public short getCount() {
        return count;
    }


    /**
     * Sets the count value for this AssessmentTemplateList.
     * 
     * @param count
     */
    public void setCount(short count) {
        this.count = count;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AssessmentTemplateList)) return false;
        AssessmentTemplateList other = (AssessmentTemplateList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.assessmentList==null && other.getAssessmentList()==null) || 
             (this.assessmentList!=null &&
              java.util.Arrays.equals(this.assessmentList, other.getAssessmentList()))) &&
            this.count == other.getCount();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssessmentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssessmentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssessmentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getCount();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AssessmentTemplateList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AssessmentTemplateList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assessmentList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assessmentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "AssessmentTemplate"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("count");
        elemField.setXmlName(new javax.xml.namespace.QName("", "count"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
