/**
 * InitMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class InitMessage  implements java.io.Serializable {
    private java.lang.String appTextLogin;

    private java.lang.String appTextLoginFail;

    public InitMessage() {
    }

    public InitMessage(
           java.lang.String appTextLogin,
           java.lang.String appTextLoginFail) {
           this.appTextLogin = appTextLogin;
           this.appTextLoginFail = appTextLoginFail;
    }


    /**
     * Gets the appTextLogin value for this InitMessage.
     * 
     * @return appTextLogin
     */
    public java.lang.String getAppTextLogin() {
        return appTextLogin;
    }


    /**
     * Sets the appTextLogin value for this InitMessage.
     * 
     * @param appTextLogin
     */
    public void setAppTextLogin(java.lang.String appTextLogin) {
        this.appTextLogin = appTextLogin;
    }


    /**
     * Gets the appTextLoginFail value for this InitMessage.
     * 
     * @return appTextLoginFail
     */
    public java.lang.String getAppTextLoginFail() {
        return appTextLoginFail;
    }


    /**
     * Sets the appTextLoginFail value for this InitMessage.
     * 
     * @param appTextLoginFail
     */
    public void setAppTextLoginFail(java.lang.String appTextLoginFail) {
        this.appTextLoginFail = appTextLoginFail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InitMessage)) return false;
        InitMessage other = (InitMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.appTextLogin==null && other.getAppTextLogin()==null) || 
             (this.appTextLogin!=null &&
              this.appTextLogin.equals(other.getAppTextLogin()))) &&
            ((this.appTextLoginFail==null && other.getAppTextLoginFail()==null) || 
             (this.appTextLoginFail!=null &&
              this.appTextLoginFail.equals(other.getAppTextLoginFail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAppTextLogin() != null) {
            _hashCode += getAppTextLogin().hashCode();
        }
        if (getAppTextLoginFail() != null) {
            _hashCode += getAppTextLoginFail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InitMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "InitMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("appTextLogin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AppTextLogin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("appTextLoginFail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AppTextLoginFail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
