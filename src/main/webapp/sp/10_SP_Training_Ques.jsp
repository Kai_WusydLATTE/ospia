<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Training Questions</title>
<c:import url="/common.jsp"></c:import>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/js/sp/10_SP_Training_Ques.js"></script>
</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>

        <a href="<%=request.getContextPath()%>/sp/scenario/${scenario.id}" style="font-size:14px">Back</a>
        
		<form id="Survey_SOCA" name="Survey_SOCA" action="spSOCA_submit">
			<input id="sceId" name="sceId" type="hidden" value="${scenario.id}">
			
			<header class="info">
				<h1>Training questions for scenario ${scenario.code}</h1>
			</header>

			<ul>

				<li id="foli218" class="notranslate  twoColumns     ">
					<fieldset>
						<![if !IE | (gte IE 8)]>
						<legend id="title218" class="desc"> <p>Question 1 </p></legend>
						<![endif]>
						<!--[if lt IE 8]>
<label id="title218" class="desc">
Question 1
</label>
<![endif]-->
						<div>
							<input id="radioDefault_218" name="Field218" type="hidden"
								value="" /> <span> <input id="Field218_0"
								name="Field218" type="radio" class="field radio"
								value="First Choice" tabindex="8" /> <label class="choice"
								for="Field218_0"> First Choice</label>
							</span> <span> <input id="Field218_1" name="Field218"
								type="radio" class="field radio" value="Second Choice"
								tabindex="9" /> <label class="choice" for="Field218_1">
									Second Choice</label>
							</span> <span> <input id="Field218_2" name="Field218"
								type="radio" class="field radio" value="Third Choice"
								tabindex="10" /> <label class="choice" for="Field218_2">
									Third Choice</label>
							</span> <span> <input id="Field218_3" name="Field218"
								type="radio" class="field radio" value="Forth Choice"
								tabindex="11" /> <label class="choice" for="Field218_3">
									Forth Choice</label>
							</span>
						</div>
					</fieldset>
				</li>
				
				
				
				<li id="foli219" class="notranslate  twoColumns     ">
					<fieldset>
						<![if !IE | (gte IE 8)]>
						<legend id="title219" class="desc"> <p>Question 2 </p></legend>
						<![endif]>
						<!--[if lt IE 8]>
<label id="title219" class="desc">
Question 1
</label>
<![endif]-->
						<div>
							<input id="radioDefault_219" name="Field219" type="hidden"
								value="" /> <span> <input id="Field219_0"
								name="Field219" type="radio" class="field radio"
								value="First Choice" tabindex="8" /> <label class="choice"
								for="Field219_0"> First Choice</label>
							</span> <span> <input id="Field219_1" name="Field219"
								type="radio" class="field radio" value="Second Choice"
								tabindex="9" /> <label class="choice" for="Field219_1">
									Second Choice</label>
							</span> <span> <input id="Field219_2" name="Field219"
								type="radio" class="field radio" value="Third Choice"
								tabindex="10" /> <label class="choice" for="Field219_2">
									Third Choice</label>
							</span> <span> <input id="Field219_3" name="Field219"
								type="radio" class="field radio" value="Forth Choice"
								tabindex="11" /> <label class="choice" for="Field219_3">
									Forth Choice</label>
							</span>
						</div>
					</fieldset>
				</li>
				
				
				<li id="foli220" class="notranslate  twoColumns     ">
					<fieldset>
						<![if !IE | (gte IE 8)]>
						<legend id="title220" class="desc"> <p>Question 3 </p></legend>
						<![endif]>
						<!--[if lt IE 8]>
<label id="title220" class="desc">
Question 1
</label>
<![endif]-->
						<div>
							<input id="radioDefault_220" name="Field220" type="hidden"
								value="" /> <span> <input id="Field220_0"
								name="Field220" type="radio" class="field radio"
								value="First Choice" tabindex="8" /> <label class="choice"
								for="Field220_0"> First Choice</label>
							</span> <span> <input id="Field220_1" name="Field220"
								type="radio" class="field radio" value="Second Choice"
								tabindex="9" /> <label class="choice" for="Field220_1">
									Second Choice</label>
							</span> <span> <input id="Field220_2" name="Field220"
								type="radio" class="field radio" value="Third Choice"
								tabindex="10" /> <label class="choice" for="Field220_2">
									Third Choice</label>
							</span> <span> <input id="Field220_3" name="Field220"
								type="radio" class="field radio" value="Forth Choice"
								tabindex="11" /> <label class="choice" for="Field220_3">
									Forth Choice</label>
							</span>
						</div>
					</fieldset>
				</li>


				<li class="buttons ">
					<div>
						<input id="submitAnswer" name="submitAnswer"
							class="usyd-ui-button-primary" type="button" value="Submit" />
					</div>
				</li>

				<li class="hide"><label for="comment">Do Not Fill This
						Out</label> <textarea name="comment" id="comment" rows="1" cols="1"></textarea>
					<input type="hidden" id="idstamp" name="idstamp"
					value="cGiw7XDVJ61c1PupHNZReMGpotcRLgHz3ypyJA+gKAg=" /></li>
			</ul>
		</form>

		<c:import url="/footer.jsp"></c:import>

	</div>

</body>
</html>
