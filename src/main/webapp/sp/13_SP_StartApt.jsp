<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Baseline</title>
<c:import url="/common.jsp"></c:import>
<!-- <script src='<%=request.getContextPath()%>/js/opentok.min.js'></script> -->
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script type="text/javascript">
	var patientSessionIdBaseline = "${patientSessionIdBaseline}";
	var patientApiKeyBaseline = "${patientApiKeyBaseline}";
	var patientTokenBaseline = "${patientTokenBaseline}";
</script>
<script src="<%=request.getContextPath()%>/js/sp/13_SP_StartApt.js"></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input id="patientArchiveIdBaseline" value="" type="hidden">
						<div class="row">
							<div style="height:700px;">
								<h2>This is a sound check page.</h2>
								<!--  is not a part of your evaluation and  -->
                                <p>The system will make a recording which will not be seen by others. Please always accept requests to share your camera and microphone.</p>
                                <p>Until the following task is completed, you will not see the student.</p> 
                                <p><b>Task: Please use several sentences to describe the weather today.</b></p>
                                <p>When you are ready, please click the "Start" button below, speak, and when finished please click the "Stop" button.</p>
                                <div id="baselineContainer"></div>
								<br> <input type="button" id="startBaseline" class="usyd-ui-button-primary" value="Start" disabled='disabled'/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>
</body>
</html>
