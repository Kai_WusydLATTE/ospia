<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Thanks</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/sp/14_SP_SOCA.js"></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="jumbotron">
							<p class="lead">
                                Please contact on <a href="mailto:csadmin@unsw.edu.au">csadmin@unsw.edu.au</a> and let us know about the problem you experienced during this OSPIA session.
                            </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
