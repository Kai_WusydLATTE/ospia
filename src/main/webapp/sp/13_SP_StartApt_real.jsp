<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Conversation</title>
<c:import url="/common.jsp"></c:import>
<!-- <script src='<%=request.getContextPath()%>/js/opentok.min.js'></script> -->
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script type="text/javascript">
	var apiKey = "${apiKey}";
	var sessionId = "${sessionId}";
	var token = "${token}";
</script>

<script src="<%=request.getContextPath()%>/js/sp/13_SP_StartApt_real.js"></script>
</head>

<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<input type="hidden" id="hiddenArchiveId" />

						<div>
							<p id="doctorLoginTime" style="color:red; display:none"></p>
						</div>

						<div>
							<a href="<%=request.getContextPath()%>/files/Scenarios.pdf" style="font-size: 16px;" target="_blank">Review this scenario</a>
						</div>


						<div class="container">


							<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;border:0" class="borderless">
								<tbody>
									<tr>
										<td style="width: 65%; vertical-align: top;">
											<div id="participantImageTitle">
												<h4>Student</h4>
											</div>
											<div id="oppositeContainerText">
												<br>
												<h3 id="noShownNote">Please share your camera and microphone if requested. <br/><br/> Thanks for participating in this interaction, please wait whilst the call connects. Thank you.</h3>
											</div>
											<div id="fullScreenDiv" style="display:none">
												<p id="timer"
													style="font-size:20px;font-weight:bold;color:red;text-align:center; background-color:black;padding:0px;margin:0px;width:640px;">0
													Minute</p>
											</div>
											<div id="oppositeContainer"></div> <br />
											<div>
												<c:if test="${apt.evaluationImage == 1 }">
													<img alt="" src="<%=request.getContextPath()%>/img/thumbup.png"
														style="width:45px; height:40px; cursor:pointer; display:inline" onclick="fingerClick(1)"> &nbsp;&nbsp;&nbsp; 
                           <img alt="" src="<%=request.getContextPath()%>/img/thumbdown.png"
														style="width:45px; height:40px; cursor:pointer; display:inline;" onclick="fingerClick(2)">
												</c:if>

												<c:if test="${apt.evaluationImage != 1 }">
													<img alt="" src="<%=request.getContextPath()%>/img/face_smile.png"
														style="width:45px; height:40px; cursor:pointer; display:inline" onclick="fingerClick(1)"> &nbsp;&nbsp;&nbsp; 
                           <img alt="" src="<%=request.getContextPath()%>/img/face_sad.png"
														style="width:45px; height:40px; cursor:pointer; display:inline;" onclick="fingerClick(2)">
												</c:if>


											</div>
											<p id="addNoteInfo" style="color:red"></p>
											<h3>Notes/Comments</h3>
											<div>
												<textarea id="comment" style="width:400px;"></textarea>
												<button id="submitComment" class="usyd-ui-button-primary" style="vertical-align:bottom;">Post Comment</button>
											</div>
										</td>
										<td style="width: 3%; vertical-align: top;"></td>
										<td style="width: 32%; vertical-align: top;">
											<div id="yourImageTitle">
												<h4>Simulated Patient</h4>
											</div>
											<div id="selfContainer">This is the image of yourself</div>
											<h5>Remember to look for:</h5>
											<p>1. Providing structure</p>
											<p>2. Gathering information</p>
											<p>3. Building relationships & developing rapport</p>
											<p>4. Ensuring a shared understanding of patient's needs and perspective/impact of problem</p>
											<h5>Technical issues:</h5>
											<p>1. To improve sound and avoid echo, preferably use ear- or head-phones, or keep your volume to the lowest audible.</p>
											<p>2. If you experience an interruption, please refresh this page.</p>
											<p>3. It is not possible to re-start an interview - you will continue from when the interruption occurred. In this case the timer will be inaccurate, and so it will be useful to keep track of for how much longer you will continue.</p> 
											<input type="button" id="stopsession" class="usyd-ui-button-primary btn-lg" value="Cease Interaction">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
