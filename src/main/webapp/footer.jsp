<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title></title>
  </head>
  
  <body>
    <div id="wrapper-bl">
        <div class="clearfix">
            <div id="footer-links">
                <div id="block-menu-menu-footer-links" class="block block-menu clearfix">


                    <div class="content">
                        <ul class="menu">
                            <li class="first expanded"><a href="https://med.unsw.edu.au/current-students" title="Link to Current Students">Students</a>
                                <ul class="menu">
                                    <li class="first leaf"><a href="https://emed.med.unsw.edu.au/" title="eMed">eMed</a></li>
                                    <li class="leaf"><a href="http://my.unsw.edu.au/" title="myUNSW">myUNSW</a></li>
                                    <li class="leaf"><a href="https://zmail.unsw.edu.au/" title="zMail - student email">zMail</a></li>
                                    <li class="leaf"><a href="https://moodle.telt.unsw.edu.au/login/index.php" title="Moodle">Moodle</a></li>
                                    <li class="leaf"><a href="http://medprogram.med.unsw.edu.au/" title="Medicine program site">Medicine program site</a></li>
                                    <li class="last leaf"><a href="https://med.unsw.edu.au/policies" title="Policies - all students">Policies - all
                                            students</a></li>
                                </ul></li>
                            <li class="expanded"><a href="http://www.unsw.edu.au/life" title="Life at UNSW">Life at UNSW</a>
                                <ul class="menu">
                                    <li class="first leaf"><a href="http://medsoc.org.au/" title="UNSW Medical Students Society">MedSoc</a></li>
                                    <li class="leaf"><a href="http://studentlife.unsw.edu.au/" title="Student Life">Student Life</a></li>
                                    <li class="leaf"><a href="http://www.arc.unsw.edu.au/" title="Arc">Arc</a></li>
                                    <li class="leaf"><a href="https://student.unsw.edu.au/ahegs" title="UNSW Advantage">UNSW Advantage</a></li>
                                    <li class="leaf"><a href="http://www.rc.unsw.edu.au/" title="Residential Communities">Residential Communities</a></li>
                                    <li class="last leaf"><a href="http://www.studentconnection.unsw.edu.au/" title="Student Connection">Student
                                            Connection</a></li>
                                </ul></li>
                            <li class="expanded"><a href="https://med.unsw.edu.au/staff" title="For Staff">For Staff</a>
                                <ul class="menu">
                                    <li class="first leaf"><a href="https://med.unsw.edu.au/learning-teaching" title="Learning &amp; Teaching">Learning
                                            &amp; Teaching</a></li>
                                    <li class="leaf"><a href="https://www.it.unsw.edu.au/" title="UNSW IT Services">IT Services</a></li>
                                    <li class="leaf"><a href="https://mail.unsw.edu.au/" title="Staff web mail interface">Webmail</a></li>
                                    <li class="leaf"><a href="https://med.unsw.edu.au/information-conjoint-staff" title="Information for Conjoint Staff">Conjoint
                                            Staff</a></li>
                                    <li class="leaf"><a href="https://med.unsw.edu.au/post-doctoral-staff" title="Post Doctoral Staff">Post Doctoral
                                            Staff</a></li>
                                    <li class="last leaf"><a href="http://www.library.unsw.edu.au/about/corporate/outreach.html"
                                        title="Outreach Librarian">Outreach Librarian</a></li>
                                </ul></li>
                            <li class="last expanded"><a href="https://med.unsw.edu.au/" title="Directories to find out more">Find out more</a>
                                <ul class="menu">
                                    <li class="first leaf"><a href="https://med.unsw.edu.au/find-a-person" title="Directory to UNSW Medicine">Find a
                                            person</a></li>
                                    <li class="leaf"><a href="https://med.unsw.edu.au/schools-centres-institutes" title="Faculty Units and Services">Units
                                            and Services</a></li>
                                    <li class="leaf"><a href="https://med.unsw.edu.au/schools-centres-institutes"
                                        title="Schools, Centres &amp; Institutes">Schools, Centres &amp; Institutes</a></li>
                                    <li class="leaf"><a href="http://www.unsw.edu.au/contacts" title="UNSW Contacts Directory">UNSW Contacts Directory</a></li>
                                    <li class="leaf"><a href="https://my.unsw.edu.au/student/atoz/AtoZstaff.html" title="UNSW Services Directory">UNSW
                                            Services Directory</a></li>
                                    <li class="last leaf"><a href="http://www.library.unsw.edu.au/" title="UNSW Library">UNSW Library</a></li>
                                </ul></li>
                        </ul>
                    </div>
                </div>
                <div id="block-block-24" class="block block-block clearfix">


                    <div class="content">
                        <div class="footer-c5">
                            <div id="contact-details">
                                <div class="inner-left">
                                    <span style="font-weight: bold;">UNSW Medicine</span>
                                    <p>
                                        <strong>CRICOS Provider Code</strong>: 00098G<br> <strong> ABN</strong>: 57 195 873 179
                                    </p>
                                </div>
                                <div class="inner-right">
                                    <h4>Postal Address</h4>
                                    <p>
                                        Faculty of Medicine<br> UNSW Australia<br> UNSW Sydney NSW 2052<br> Australia
                                    </p>
                                </div>
                                <div class="inner-left">
                                    <h4>Key Contact</h4>
                                    <p>
                                        Kiran Thwaites<br>
                                        <b>T</b> +61 (2) 9385 2550<br><b>E</b> csadmin@unsw.edu.au
                                    </p>
                                </div>
                                
                                <div class="inner-right">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Footer Links -->
            </div>
        </div>
    </div>
    <!-- End Wrapper Bottom Links -->


    <!-- Start Wrapper Bottom -->
    <div id="wrapper-b">
        <div id="footer">
            <div class="footer-t">
                <div class="footer-tl">
                    <!-- Start Footer Menu-->
                    <div id="footer-menu" class="footer-menu">
                        <div class="region region-footer-bottom-links">
                            <div id="block-menu-menu-footer-bottom-menu" class="block block-menu clearfix">


                                <div class="content">
                                    <ul class="menu">
                                        <li class="first leaf"><a href="http://www.unsw.edu.au/privacy" title="">Privacy Policy</a></li>
                                        <li class="leaf"><a href="http://www.unsw.edu.au/copyright-disclaimer" title="">Copyright and Disclaimer</a></li>
                                        <li class="leaf"><a href="http://www.unsw.edu.au/accessibility" title="">Accessibility</a></li>
                                        <li class="leaf"><a href="https://med.unsw.edu.au/enquiry-form" title="Enquiry and Feedback Form">Online Enquiry</a></li>
                                        <li class="leaf"><a href="https://med.unsw.edu.au/sitemap" title="">Sitemap</a></li>
                                        <li class="last leaf"><a href="https://med.unsw.edu.au/user/login" title="">Log in</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Footer Menu-->
                </div>

                <div class="footer-tr">
                    <div class="region region-footer-quick-links">
                        <div id="block-block-103" class="block block-block clearfix">


                            <div class="content">
                                <ul class="quick-link">
                                    <li><a class="facebook" href="https://www.facebook.com/UNSWMedicine" title="Facebook">Facebook</a></li>
                                    <li><a class="rssfeeds" href="https://med.unsw.edu.au/news/feed" title="Subscribe to News RSS">News RSS</a></li>
                                    <li><a class="youtube" href="http://www.youtube.com/course?list=EC7F8133E78C7E75E2&feature=plcp"
                                        title="Follow us on YouTube">YouTube</a></li>
                                    <li><a class="itunesu" href="https://itunes.apple.com/au/itunes-u/health/id423492216" title="iTunes U">iTunes U</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-b">
                <div class="footer-bl">
                    <div class="region region-footer-copyright">
                        <div id="block-block-105" class="block block-block clearfix">


                            <div class="content">
                                <div>Copyright &copy; 2012 UNSW MEDICINE</div>
                                <div>Authorised by Office of the Dean</div>
                                <div>Page Last Updated: 2:30:54 pm, Monday 07 December 2015</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </body>
</html>
