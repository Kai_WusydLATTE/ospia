<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Baseline</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input id="doctorArchiveIdBaseline" value="" type="hidden">
						<div style="height:800px;">
							
							<div class="jumbotron" style="padding-top:22px;padding-bottom:2px;">
	                            <h2>Assessment Training and Calibration</h2> <br>
	                            <h5><a target="_tab" href="<%=request.getContextPath()%>/files/ResearchConsentForm_SP.pdf"> <img
                                    class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png"> <span class="pagefont">Research consent
                                        form</span>
                                     </a>
                                     </h5>
	                        </div>
							
							<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;border:0" class="borderless">
                            <tbody>
                                <tr>
                                    <td style="width: 30%; vertical-align: top;">
                                        <p>
                                            <a target="_blank" href="<%=request.getContextPath()%>/video/cali_1.mp4"> <img alt=""
                                                src="<%=request.getContextPath()%>/img/cali_good.png" class="trainingimg">
                                            </a>
                                            <h3>Interview 1</h3>
                                            <a href="<%=request.getContextPath()%>/cali/soca_cali/${calibrationUser.id},1" target="_blank"> 
                                                 1. Assess this interview
			                                </a>
			                                <br/>
			                                <!-- 
			                                <a href="<%=request.getContextPath()%>/cali/reliability/${calibrationUser.id}/1" target="_blank"> 
                                                 Check the reliability of my assessment
                                            </a>
                                             -->
                                             <a href="<%=request.getContextPath()%>/cali/standard/${calibrationUser.id}/1" target="_blank"> 
                                                 2. Check tutor's assessment
                                            </a>
                                        </p>
                                    </td>
                                    <td style="width: 5%; vertical-align: top;"></td>
                                    <td style="width: 30%; vertical-align: top;">
                                        <p>
                                            <a target="_blank" href="<%=request.getContextPath()%>/video/cali_2.mp4"> <img alt=""
                                                src="<%=request.getContextPath()%>/img/cali_neutral.png" class="trainingimg">
                                            </a>
                                            <h3>Interview 2</h3>
                                            <a href="<%=request.getContextPath()%>/cali/soca_cali/${calibrationUser.id},2" target="_blank"> 
                                                 1. Assess this interview
                                            </a>
                                            <br/>
                                           <!-- 
                                            <a href="<%=request.getContextPath()%>/cali/reliability/${calibrationUser.id}/2" target="_blank"> 
                                                 Check the reliability of my assessment
                                            </a>
                                             -->
                                            <a href="<%=request.getContextPath()%>/cali/standard/${calibrationUser.id}/2" target="_blank"> 
                                                 2. Check tutor's assessment
                                            </a>
                                        </p>
                                        
                                    </td>
                                    <td style="width: 5%; vertical-align: top;"></td>
                                    <td style="width: 30%; vertical-align: top;">
                                        <p>
                                            <a target="_blank" href="<%=request.getContextPath()%>/video/cali_3.mp4"> <img alt=""
                                                src="<%=request.getContextPath()%>/img/cali_bad.png" class="trainingimg">
                                            </a>
                                            <h3>Interview 3</h3>
                                            <a href="<%=request.getContextPath()%>/cali/soca_cali/${calibrationUser.id},3" target="_blank"> 
                                                 1. Assess this interview
                                            </a>
                                            <br/>
                                            <!-- 
                                            <a href="<%=request.getContextPath()%>/cali/reliability/${calibrationUser.id}/3" target="_blank"> 
                                                 2. Compare with tutor assessment
                                            </a>
                                             -->
                                            <a href="<%=request.getContextPath()%>/cali/standard/${calibrationUser.id}/3" target="_blank"> 
                                                 2. Check tutor's assessment
                                            </a>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
							
						<div>
						<p style="font-size:16px;"><b>Instructions</b></p> 
						<p>1. Please click on the thumbnail of the video to view. Some videos may take up to 30 seconds to load, please be patient.</p>
                        <p>2. Please view the videos in their entirety. This will help you make a fair assessment of all the skills each student demonstrates.</p>
                        <p>3. Once you have viewed one video, please click on the "Assess this interview" link below it.</p>
                        <p>4. In the new tab that opens, you are presented with the SOCA assessment form. Please fill this in according to the instructions in the "Assessing the Student" training video found on this page (below)</p>
                        <p> <a href="https://vimeo.com/152970934" target="_tab">Click here to watch the "Assessing the Student" training video. </a></p>
                        <p>5. Once you have completed your assessment, please click Submit. You will then be offered the chance to compare your assessment with that of a tutor's assessment of the same interaction.</p>
                        <p>6. Continue with the remaining two videos. We hope that in doing so, you will feel comfortable assessing students in future interactions!</p>
						</div>	
							
						</div>
					</div>
					
					<div id="waitingPopup" style="display:none">
                            <h4>We are generating your video. Please wait a moment.</h4>
                    </div>

				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
