$(document).ready(function() {

	var doctorId = $("#doctorId").val();
	
	$("#submitAgreeScenario").click(function() {
		$.ajax({
			type : 'POST',
		    headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/finishtraining/'+doctorId,
			dataType : "json",
			success : function(data) {
				if(data.code==1){
					alert("Operation success");
					$("#agreeScenarioDiv").hide();
				}else{
					
				}
			},
			error: function() { 
				 alert("Some problem occured, please refresh this page");
		    }  
		});
	});
});