
var accessingDoctorTimeInterval;

$(document)
		.ready(
				function() {

					connectionCount = 0;
					if (OT.checkSystemRequirements() == 0) {
						OT.log("The client does not support WebRTC.");
					}

					// --------------------- this is the session of original
					// session: start-------------------------------
					session = OT.initSession(apiKey, sessionId);

					session
							.on({
								connectionCreated : function(event) {
									console.log(connectionCount
											+ " connections.");
								},

								connectionDestroyed : function(event) {
									console.log(connectionCount
											+ " connections.");
								},

								sessionDisconnected : function sessionDisconnectHandler(
										event) {
									// The event is defined by the
									// SessionDisconnectEvent class
									if (event.reason == "networkDisconnected") {
										alert("Your network connection terminated.");
									}
								},

								streamCreated : function(event) {
									console.log("New stream in the session: "
											+ event.stream.streamId);
									var subscriberProperties = {
										// resolution : "1280x720",
										resolution : "640x480",
										insertMode : "append",
										width : 640,
										height : 480
									};
									session.subscribe(event.stream,
											'oppositeContainer',
											subscriberProperties, function(
													error) {
												if (error) {
													alert(error);
												} else {
													$('#oppositeContainerText')
															.remove();
													start();
												}
											});
								},
							});

					session
							.connect(
									token,
									function(error) {
										if (error) {
											alert("Unable to connect: "
													+ error.code + ", "
													+ error.message);
											return;
										}

										OT.log("Connected to the session.");

										if (session.capabilities.publish != 1) {
											alert("You cannot publish an audio-video stream.");
											return;
										}

										var publisherProperties = {
											resolution : "640x480",
											width : 280,
											height : 210
										};
										publisher = OT
												.initPublisher(
														'selfContainer',
														publisherProperties,
														function(error) {
															if (error) {
																OT
																		.log(
																				"Unable to publish stream: ",
																				error.message);
																return;
															}

															publisher
																	.on({
																		streamCreated : function(
																				event) {
																			OT
																					.log("Publisher started streaming.");
																		},
																		streamDestroyed : function(
																				event) {
																			OT
																					.log("Publisher stopped streaming. Reason: "
																							+ event.reason);
																		}
																	});

															if (publisher) {
																session
																		.publish(publisher);
															}
														});
									});

//					$("#stopsession").click(function() {
//						stop();
//					});
					
					$("#stopsession").one("click",function() {
						stop();
					});
					
					$("#submitComment").click(function() {
						var comment = $("#comment").val();
						submitComment(comment);
						
					});
					
					// requestServer();

					accessingDoctorTimeInterval = setInterval(accessDoctorLoginTime, 10000);
					
					displayRecordingTime = setInterval(displayRecordingTime, 10000);
				});

function requestServer() {
	setTimeout(function() {
		checkDoctorSession();
		requestServer();
	}, 2000);
}

function checkDoctorSession() {

	$.ajax({
		type : 'GET',
		url : '/'+DOCTORPATH+'/doctorlogintime/11',
		dataType : "json",
		success : function(data) {
			if (date != null) {
				if (data.code == 1) {
					$("#doctorLoginTime").text(
							"The student logged in the system at: "
									+ data[0].content);
					$("#doctorLoginTime").show();
				} else {
					// $("#errorInfo").show();
				}
			}
		}
	});
}

// start the video archiving
function start() {
	var sessionIdStr = sessionId + '__1';
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/start/' + sessionIdStr,
		dataType : "json",
		success : function(data) {
			$("#hiddenArchiveId").val(data[0]);
			// requestTimer();
		}
	});
}

// stop the video archiving
function stop() {

	var url = sessionId;
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/stop/' + url,
		dataType : "json",
		success : function(data) {
//			if (data.code == 1) {
//				window.location.assign("/"+PATIENTPATH+"/soca_sp");
//			} else {
//				alert(data.content);
//			}
			
			if (data.code == 1) {
				window.location.assign("/"+PATIENTPATH+"/appropriate/"+sessionId);
			} else {
				alert(data.content);
			}
			
			
		}
	});
}


function fingerClick(type) {
	var realTime = new Date().getTime();
	var noteContent = type;
	var formdata = {
		"sessionId" : sessionId,
		"realTime" : realTime,
		"noteContent" : noteContent
	};

	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/addnotepatient',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(formdata),
		success : function(data) {
			if (data.code == 1) {
				$("#addNoteInfo").text("Add note success");
				setTimeout(function() {
					$("#addNoteInfo").text("");
				}, 2000);
			}else if (data.code == -1) {
				alert(data.content);
			} else {
				$("#errorInfo").show();
			}
		}
	});
}

function submitComment(comment){
	var realTime = new Date().getTime();
	var noteContent = comment;
	var formdata = {
		"sessionId" : sessionId,
		"realTime" : realTime,
		"noteContent" : noteContent
	};

	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+PATIENTPATH+'/addnotepatient',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(formdata),
		success : function(data) {
			if (data.code == 1) {
				$("#addNoteInfo").text("Add note success");
				$("#comment").val("");
				setTimeout(function() {
					$("#addNoteInfo").text("");
				}, 2000);
			} else if (data.code == -1) {
				alert(data.content);
			} else {
				$("#errorInfo").show();
			}
		}
	});
}



$(window).bind("beforeunload",unloadChatRoom);

function unloadChatRoom(){
	var sessionIdStr = sessionId + '__1';
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/unloadchatroom/' + sessionIdStr,
		dataType : "json",
		success : function(data) {
			console.log("patient unload chat room");
		}
	});
}


function accessDoctorLoginTime(){
	var sessionIdStr = sessionId;
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/getapt/' + sessionIdStr,
		dataType : "json",
		success : function(data) {
			if(data != null){
				var apt = data;
				if(apt.doctorLoginAt == null || apt.doctorLoginAt == 0){
					var patientLoginAt = apt.patientLoginAt;
					var curTime = new Date().getTime();
					console.log(curTime-patientLoginAt);
					// if((curTime-patientLoginAt)>900000){
					if((curTime - apt.starttime)>900000){
						
						alert("Thank you for waiting. Unfortunately this appointment has been cancelled, you can now close this window");
						clearInterval(accessingDoctorTimeInterval);
						
						$.ajax({
							type : 'POST',
							headers: getTokenHeader(),
							url : '/'+PATIENTPATH+'/doctornoshow/' + sessionId,
							dataType : "json",
							success : function(data) {
							}
						});
					}
				}else{
					clearInterval(accessingDoctorTimeInterval);
				}
			}
		}
	});
}


function displayRecordingTime(){
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/requestrecodingtime/' + sessionId,
		dataType : "json",
		success : function(data) {
			if (data.code == 1) {
				$("#timer").text("\u00A0\u00A0\u00A0"+data.content);
				//$("#timer").text("15 Minutes");
				$("#fullScreenDiv").show();
				console.log(data.content);
			} else {
				$("#fullScreenDiv").hide();
				console.log("not start");
			}
		}
	});
	
}



