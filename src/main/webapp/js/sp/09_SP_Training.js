$(document).ready(function() {

	var trainedsceids = $("#trainedsceids").val();
	if(trainedsceids===null || trainedsceids==="" ||trainedsceids=="null"){
		$("#agreeScenarioDiv").show();
		$("#caliLinkDiv").hide();
	}else{
		$("#caliLinkDiv").show();
	}
	
	
	$("#agreeScenario").change(function(){
		if($("#agreeScenario").is(':checked')){
			$("#submitAgreeScenario").removeAttr("disabled");
		}else{
			$("#submitAgreeScenario").attr("disabled","disabled");
		}	
	});
	
	$("#submitAgreeScenario").click(function(){
		var patientid = $("#patientid").val();
		var url = '/'+PATIENTPATH+'/agreescenario/' + patientid;
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : url,
			dataType : "json",
			success : function(data) {
				if (data !== null) {
					alert("Operation Success");
					window.location.reload();
				}
			}
		});
	});
	
	// user log into the system
	$("#searchBtn").click(function() {

		var content = $("#content").val();
		if (content.trim() === "") {
			alert("Please input some search content.");
			return;
		} else {
			$("#searchScenarioForm").submit();
		}
	});

});
