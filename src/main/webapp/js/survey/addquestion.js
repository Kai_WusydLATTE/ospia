$(document).ready(function() {
	
	
	// user log into the system
	$("#type").change(function() {
		var typeText =$("#type option:selected").text();
		if (hasOption(typeText)) {
			$("#allOptions").empty();
			$("#addoption").click();
			$("#allOptionsDiv").show();
		} else {
			$("#allOptionsDiv").hide();
		}
	});

	var optionNumber = 0;

	$("#addoption").click(function() {
		optionNumber++;
		var optionId = "option" + optionNumber;
		var delteOptionId = "delete" + optionNumber;

		var divElement = "<div class='questionoptions' id='"+ optionId + "'>"
				+ "<label class='col-md-1 col-sm-1 control-label' for='textinput'> option key: </label> " 
				+ "<input class='col-md-3 col-sm-3' id='optkey"+ optionNumber+ "' name='optkey' type='text' />"
				+ "<label class='col-md-1 col-sm-1 control-label' for='textinput'> option value: </label> " 
				+ "<input class='col-md-3 col-sm-3' id='optvalue"+ optionNumber+ "' name='optvalue' type='text' />"
				+ "<a onclick=deleteOption("+ optionNumber+ ") id='"+ delteOptionId + "'> <img src='/img/delete.png' class='operationicon' /></a>"
				+ "<a onclick=moveUp("+ optionNumber+ ",'option') id='"+ delteOptionId + "'> <img src='/img/up.png' class='operationicon' /></a>"
				+ "<a onclick=moveDown("+ optionNumber+ ",'option') id='"+ delteOptionId + "'> <img src='/img/down.png' class='operationicon' /></a>"
				+ "</div>";
		$("#allOptions").append(divElement);
	});

	$("#saveQuestion").click(function() {
		var question = new Object();
		var questionType = new Object();
		
		var title = $("#title").val();
		var description = $("#description").val();
		var typeId = $("#type").val();
		var typeText =$("#type option:selected").text();
		var isCompulsory = $("input[name='isCompulsory']:checked").val();	
		if(isCompulsory!=1){
			isCompulsory = 0;
		}
		questionType.id=typeId;
		questionType.type=typeText;
		
		question.title = title;
		question.description = description;
		question.typeS= typeText;
		question.type = questionType;
		question.isCompulsory = isCompulsory;
		
		if (hasOption(typeText)) {
			var options = $('[id^=option]');
			var optionsArray = new Array();
			$.each(options, function(i, val) {
				var divId = val.id;
				var idNumber = parseInt(divId.substr(6));
				console.log(idNumber);
				var tempOption = new Object();
				tempOption.optkey = $("#optkey" + idNumber)
						.val();
				tempOption.optvalue = $(
						"#optvalue" + idNumber).val();
				optionsArray.push(tempOption);
			});
			question.options = optionsArray;
		}
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/savequestion',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(question),
			success : function(data) {
				if (data.code == 1) {
					alert("save success");
					window.location.href="/"+SURVEYPATH+"/listquestion";
				} else {
					alert("save fail");
				}
			}
		});

	});

});

function deleteOption(id) {
	var divParent = $("#option" + id);
	divParent.remove();

}

function hasOption(typeText){
	if(typeText == "RADIO" || typeText == "CHECKBOX" || typeText == "LIKERT" ){
		return true;
	}
	return false;
}
