$(document).ready(function() {
	
	$("#saveSurvey").click(function() {
		var survey = new Object();
		var title = $("#title").val();
		var description = $("#description").val();
		var questions = allSelectedQuestions();
		
		
		survey.title = title;
		survey.description = description;
		survey.questions = questions;
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/savesurvey',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(survey),
			success : function(data) {
				if (data.code == 1) {
					alert("save success");
					window.location.href="/"+SURVEYPATH+"/listsurvey";
				} else {
					alert("save fail");
				}
			}
		});
		

	});

});


function allSelectedQuestions(){
	var questionsArray = new Array();
	var questions = $('[id^=qcheckbox]');
	$.each(questions, function(i, val) {
		if(val.checked){
			var questionId = val.id;
			var idNumber = parseInt(questionId.substr(9));
			var question = new Object();
			question.id = idNumber;
			questionsArray.push(question);	
		}
	});
	return questionsArray;
}


function hasOption(typeText){
	if(typeText == "RADIO" || typeText == "CHECKBOX"){
		return true;
	}
	return false;
}




